#ifndef MAP_HPP
#	define MAP_HPP

#	include <functional> // std::less
#	include <memory> 	// std::allocator
#	include <cstddef>	// std::ptrdiff_t
#	include "trees.hpp"
#	include "pair.hpp"
#	include "iterators.hpp"
#	include "lexicographical_compare.hpp"
#	include <limits>	// for max_size()

namespace ft
{

template <	class Key,
		 	class T,
			class Compare = std::less<Key>,
		  	class Alloc = std::allocator<ft::pair<const Key, T> >
		 >
class map {

	public:
		typedef Key												key_type;
		typedef	T												mapped_type;
		typedef typename ft::pair<const key_type, mapped_type>	value_type;
		typedef	Compare											key_compare;
		typedef Alloc											allocator_type;

		typedef typename allocator_type::reference				reference;
		typedef typename allocator_type::const_reference		const_reference;
		typedef	typename allocator_type::pointer				pointer;

		class value_compare : public std::binary_function<value_type, value_type, bool> {
					friend class map;

					protected:
						Compare comp;
						value_compare( Compare c ) : comp(c) {}

					public:
						bool	operator() ( const value_type& x, const value_type& y ) const {
							return (comp(x.first, y.first));
						}
				};

		typedef typename allocator_type::template
			rebind<RBT_node<value_type> >::other  node_allocator;

		typedef RBT<Key, T, value_type, value_compare, node_allocator>	Rep_type;

		typedef typename Rep_type::RBT_iterator				iterator;
		typedef typename Rep_type::const_RBT_iterator		const_iterator;
		typedef ft::reverse_iterator<iterator>				reverse_iterator;
		typedef ft::reverse_iterator<const_iterator>		const_reverse_iterator;
		typedef ptrdiff_t									difference_type;
		typedef size_t										size_type;

			
	public:

		// CONSTRUCTORS/DESTRUCTOR

		explicit map( const key_compare& comp = key_compare(),
						const allocator_type& alloc = allocator_type() ) : 
			 allocator_(alloc), val_comp_(comp), tree_(val_comp_, alloc) {}

		template <class InputIterator>
		map (InputIterator first, InputIterator last,
				const key_compare& comp = key_compare(),
				const allocator_type& alloc = allocator_type() ) :
			allocator_(alloc), val_comp_(comp) {
				while (first != last) {
					insert(*first);
					++first;
				}
			} // to be tested

		map( const map& x ) : allocator_(x.allocator_), val_comp_(x.val_comp_), tree_(x.tree_) {}

		map&	operator=( const map& rhs ) {
			this->allocator_ = rhs.allocator_;
			this->val_comp_ = rhs.val_comp_;
			this->tree_ = rhs.tree_;
			return (*this);
		}

		~map() {}

		// showing inner red black tree

		void	print_tree() const { this->tree_.print_tree(); }

		// ITERATORS

		iterator				begin() {
			return (iterator(this->tree_.begin(), this->tree_.get_sentinel()));
		}
		const_iterator			begin() const {
			return (const_iterator(this->tree_.begin(), this->tree_.get_sentinel()));
		}

		iterator				end() {
			return (iterator(this->tree_.end(), this->tree_.get_sentinel()));
		}
		const_iterator 			end() const {
			return (const_iterator(this->tree_.end(), this->tree_.get_sentinel()));
		}

		reverse_iterator 		rbegin() {
			return (reverse_iterator(this->end()));
		}
		const_reverse_iterator	rbegin() const {
			return (const_reverse_iterator(this->end()));
		}

		reverse_iterator 		rend() {
			return (reverse_iterator(this->begin()));
		}
		const_reverse_iterator	rend() const {
			return (const_reverse_iterator(this->begin()));
		}

		// CAPACITY

		bool		empty() const { return (size() == 0);}

		size_type	size() const { return (this->tree_.get_size()); }

		size_type	max_size() const {
			return ( 2 * std::numeric_limits<difference_type>::max() / sizeof(RBT_node<value_type>) );
		}

		// ELEMENT ACCESS

		mapped_type&	operator[] ( const key_type& k ) {
			return ( (*((this->insert(ft::make_pair(k,mapped_type()))).first)).second );
		}

		// MODIFIERS

		ft::pair<iterator, bool>	insert( const value_type& val) {
			bool is_inserted;
			iterator it_insert = this->tree_.insert(val, &is_inserted);
			ft::pair<iterator, bool> ret_pair = ft::make_pair(it_insert, is_inserted);
			return (ret_pair);
		}
		iterator					insert( iterator position, const value_type& val ) {
			iterator it = this->tree_.insert_with_hint(val, &position);
			return (it);
		}
		template <class InputIterator>
		void						insert(InputIterator first, InputIterator last) {
			bool is_inserted;
			while (first != last) 
				this->tree_.insert(*first++, &is_inserted);
		}

		void						erase( iterator position ) {
			this->tree_.erase(position);
		}
		size_type					erase( const key_type& k ) {
			iterator target = find(k);
			if (target == this->end())
				return (0);
			erase(target);
			return (1);
		}
		void						erase( iterator first, iterator last) {
			iterator tmp;
			while (first != last) {
				tmp = first;
				++first;
				erase(tmp);
			}
		}

		void						swap( map& x ) {
			this->tree_.swap(x.tree_);
		}

		void						clear() { this->tree_.clear(); }

		// OBSERVERS

		key_compare		key_comp() const { return (this->val_comp_.comp); }
		value_compare	value_comp() const { return (this->val_comp_); }

		// OPERATIONS

		iterator		find( const key_type& k ) {
			value_type target = ft::pair<key_type, mapped_type>(k, mapped_type());
			return (this->tree_.find(target));
		}
		const_iterator	find( const key_type& k ) const {
			value_type target = ft::pair<key_type, mapped_type>(k, mapped_type());
			return (this->tree_.find(target));
		}

		size_type		count( const key_type& k ) const {
			value_type target = ft::pair<key_type, mapped_type>(k, mapped_type());
			return (this->tree_.find(target) == this->end() ? 0 : 1);
		}

		iterator		lower_bound( const key_type& k ) {
			value_type target = ft::pair<key_type, mapped_type>(k, mapped_type());
			return (this->tree_.lower_bound(target));
		}
		const_iterator	lower_bound( const key_type& k ) const {
			value_type target = ft::pair<key_type, mapped_type>(k, mapped_type());
			return (this->tree_.lower_bound(target));
		}
		
		iterator		upper_bound( const key_type& k ) {
			value_type target = ft::pair<key_type, mapped_type>(k, mapped_type());
			return (this->tree_.upper_bound(target));
		}
		const_iterator	upper_bound( const key_type& k ) const {
			value_type target = ft::pair<key_type, mapped_type>(k, mapped_type());
			return (this->tree_.upper_bound(target));
		}

		ft::pair<iterator, iterator>				equal_range( const key_type& k ) {
			return (ft::pair<iterator, iterator>(lower_bound(k), upper_bound(k)));
		}
		ft::pair<const_iterator, const_iterator>	equal_range( const key_type& k ) const {
			return (ft::pair<const_iterator, const_iterator>(lower_bound(k), upper_bound(k)));
		}

		// ALLOCATOR

		allocator_type	get_allocator() const { return (this->allocator_); }

	private:
		allocator_type	allocator_;
		value_compare	val_comp_;
		Rep_type		tree_;

}; // map

// NON-MEMBER OVERLOADS

// relational operators

template< class Key, class T, class Compare, class Alloc >
bool	operator==( const map<Key, T, Compare, Alloc> lhs,
					const map<Key, T, Compare, Alloc> rhs ) {
	typedef typename  map<Key, T, Compare, Alloc>::const_iterator const_it;
	if ( lhs.size() == rhs.size() ){
		const_it rhs_it = rhs.begin();
		const_it lhs_it = lhs.begin();
		while (lhs_it != lhs.end()) {
			if (*lhs_it != *rhs_it)
				return (false);
			++lhs_it;
			++rhs_it;
		}
		return (true);
	}
	else
		return (false);
}

template< class Key, class T, class Compare, class Alloc >
bool	operator!=( const map<Key, T, Compare, Alloc> lhs,
					const map<Key, T, Compare, Alloc> rhs ) {
	return (!(lhs == rhs));
}

template< class Key, class T, class Compare, class Alloc >
bool	operator<( const map<Key, T, Compare, Alloc> lhs,
					const map<Key, T, Compare, Alloc> rhs ) {
	return (ft::lexicographical_compare(lhs.begin(), lhs.end(),
										rhs.begin(), rhs.end()));
}

template< class Key, class T, class Compare, class Alloc >
bool	operator<=( const map<Key, T, Compare, Alloc> lhs,
					const map<Key, T, Compare, Alloc> rhs ) {
	return ( !(rhs < lhs) );
}

template< class Key, class T, class Compare, class Alloc >
bool	operator>( const map<Key, T, Compare, Alloc> lhs,
					const map<Key, T, Compare, Alloc> rhs ) {
	return ( rhs < lhs );
}

template< class Key, class T, class Compare, class Alloc >
bool	operator>=( const map<Key, T, Compare, Alloc> lhs,
					const map<Key, T, Compare, Alloc> rhs ) {
	return ( !(lhs < rhs) );
}

// swap

template <class Key, class T, class Compare, class Alloc>
void	swap( map<Key, T, Compare, Alloc>&x, map<Key, T, Compare, Alloc>& y ) {
	x.swap(y);
}

} // namespace ft

#endif
