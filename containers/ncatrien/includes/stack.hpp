#ifndef STACK_HPP
#	define STACK_HPP

#	include "vector.hpp"

namespace ft
{

template <class T, class Container = ft::vector<T> >
class stack {

	public:
		typedef Container 							container_type;
		typedef typename Container::value_type		value_type;
		typedef	typename Container::size_type 		size_type;
		typedef	typename Container::reference		reference;
		typedef typename Container::const_reference	const_reference;

	protected:
		container_type	c;

	public:

		// MEMBER FUNCTIONS
		//
		// constructor - destructor
		explicit	stack( const container_type& ctnr = container_type() ) : c(ctnr) {};

		~stack() {}

		stack&	operator=( const stack& other ) {
			this->c = other.c;
			return (*this);
		}

		// element access
	
		reference		top() { return (c.back()); }
		const_reference	top() const { return (c.back()); }

		// capacity

		bool		empty() const { return (c.empty()); }
		size_type	size() const { return (c.size()); }

		// modifiers

		void	push( const value_type& value ) { c.push_back(value); }
		void	pop() { c.pop_back(); }

};

// NON-MEMBER FUNCTIONS OVERLOADS

template <class T, class Container>
bool	operator==( const stack<T, Container>& lhs, const stack<T, Container>& rhs ) {
	return ( lhs.c == rhs.c );
}

template <class T, class Container>
bool	operator!=( const stack<T, Container>& lhs, const stack<T, Container>& rhs ) {
	return ( lhs.c != rhs.c );
}

template <class T, class Container>
bool	operator<( const stack<T, Container>& lhs, const stack<T, Container>& rhs ) {
	return ( lhs.c < rhs.c );
}

template <class T, class Container>
bool	operator<=( const stack<T, Container>& lhs, const stack<T, Container>& rhs ) {
	return ( lhs.c <= rhs.c );
}

template <class T, class Container>
bool	operator>( const stack<T, Container>& lhs, const stack<T, Container>& rhs ) {
	return ( lhs.c > rhs.c );
}

template <class T, class Container>
bool	operator>=( const stack<T, Container>& lhs, const stack<T, Container>& rhs ) {
	return ( lhs.c > rhs.c );
}

} // namespace ft
#endif
