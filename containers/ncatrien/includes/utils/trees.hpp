#ifndef TREES_HPP

#	define TREES_HPP

#	include <iostream>
#	include <memory>
#	include "pair.hpp"

#	define	COLOR_RED		"\033[1;31m"	
#	define	COLOR_RESET		"\033[0m"		

namespace ft
{


enum	RBT_color { RED = false, BLACK = true };

template <class T> // T will be a pair
struct	RBT_node {

		T			value;
		RBT_node	*left;
		RBT_node	*right;
		RBT_node	*parent;
		RBT_color	color;
		
		RBT_node( const T& val ) : value(val), left(NULL), right(NULL), parent(NULL),
									color(RED) {} // a newly inserted node is red
		RBT_node( const RBT_node& x ) :
			value(x.value), left(x.left), right(x.right), parent(x.parent), color(x.color) {}
		RBT_node() : value(T()), left(NULL), right(NULL), parent(NULL), color(BLACK) {}
		~RBT_node() {}
		RBT_node&	operator=( const RBT_node& rhs ) {
		//	this->value = rhs.value; // this line is causing pb with copying pair
			this->left = rhs.left;
			this->right = rhs.right;
			this->parent = rhs.parent;
			this->color = rhs.color;
			return (*this);
		}

};

template <class Key, class T, class TypeOfValue, class Compare, class Alloc> // T will be a pair
class RBT {

	private:
		typedef Key								key_type;
		typedef	T								mapped_type;
		typedef TypeOfValue						value_type;
		typedef struct RBT_node<value_type>		node;
		typedef size_t							size_type;

	public:
		struct RBT_iterator : public std::iterator<std::bidirectional_iterator_tag, value_type> {

			typedef value_type&	reference;
			typedef value_type*	pointer;

			typedef RBT_node<value_type>*				Node_pointer;
			typedef RBT_iterator				Self;

			friend class RBT;

			RBT_iterator() : m_node(), nil() {}
			RBT_iterator( const RBT_iterator& other ) : m_node(other.m_node), nil(other.nil) {}

			explicit RBT_iterator( Node_pointer x, Node_pointer sentinel ) : m_node(x), nil(sentinel) {}

			Self&	operator=( const RBT_iterator& rhs ) {
				this->m_node = rhs.m_node;
				this->nil = rhs.nil;
				return (*this);
			}

			Self&	operator++() {
				if (this->m_node == this->nil->right)
					this->m_node = this->nil;
				else
					this->m_node = increment(this->m_node);
				return (*this);
			}
			Self	operator++(int) {
				Self copy(*this);
				operator++();
				return (copy);
			}

			Self&	operator--() {
				if (this->m_node == this->nil->left)
					this->m_node = this->nil;
				else
					this->m_node = decrement(this->m_node);
				return (*this);
			}
			Self	operator--(int) {
				Self copy(*this);
				operator--();
				return (*this);
			}	

			reference	operator*() const { return (m_node->value); }

			pointer		operator->() const { return (&(m_node->value)); }

			bool	operator==( const Self& rhs ) const {
				return (this->m_node == rhs.m_node);
			}
			bool	operator!=( const Self& rhs ) const {
				return (!(*this == rhs));
			}

			void	*get_node_address() { return (m_node); }
			node	*get_node() const { return (m_node); }

			protected:
				Node_pointer	m_node;
				Node_pointer	nil;

				node	*increment( node *x ) {
					if (x->right != this->nil) {
						x = x->right;
						while (x->left != this->nil)
							x = x->left;
					}
					else {
						node *y = x->parent;
						while (x == y->right) {
							x = y;
							y = y->parent;
						}
						if (x->right != y)
							x = y;
					}
					return (x);
				}

				node	*decrement( node *x ) {
					if (x->color == RED && x->parent->parent == x)
						x = x->right;
					// if x == end(), need to return nil->right
					if (x == this->nil && this->nil->right != NULL)
						return (this->nil->right);
					else if (x->left != this->nil) {
						node *y = x->left;
						while (y->right != this->nil)
							y = y->right;
						x = y;
					}
					else {
						node *y = x->parent;
						while (x == y->left) {
							x = y;
							y = y->parent;
						}
						x = y;
					}
					return (x);
				}
		};

		struct const_RBT_iterator : public RBT_iterator {
			typedef const value_type& reference;
			typedef const value_type* pointer;

			typedef RBT_node<value_type>*	Node_pointer;
			typedef const_RBT_iterator	Self;

			const_RBT_iterator() : RBT_iterator() {}
			const_RBT_iterator( const RBT_iterator& other ) : RBT_iterator(other) {}
			const_RBT_iterator( Node_pointer x, Node_pointer sentinel ) : RBT_iterator(x, sentinel) {}

			reference operator*() const {
				return (this->m_node->value);
			}

			pointer operator->() const {
				return (&(this->m_node->value));
			}
		};

	protected:

		Alloc		alloc_; // thks to rebound, this is a node allocator
		Compare		comp_;

		node		*root_;
		node		*nil; // this is the sentinel
		node		*max;
		node		*min;
		size_type	size_;

		void	make_empty( node *n ) {
			if (n != this->nil) {
				make_empty(n->left);
				make_empty(n->right);
				this->alloc_.deallocate(n, 1); 
			}
			this->size_ = 0;
		}

		node	*new_node( const value_type& val ) {
			node	*ret = this->alloc_.allocate(1);
			this->alloc_.construct(ret, val);
			return (ret);
		}

		node	*leftmost( node *x ) const {
			while (x != this->nil && x->left != this->nil) x = x->left;
			return (x);
		}

		node	*rightmost( node *x ) const {
			while (x != this->nil && x->right != this->nil) x = x->right;
			return (x);
		}

		node	*tree_deep_copy( const node *other, const node *other_nil ) {
			if (other == other_nil)
				return (this->nil);
			node *new_elt = new_node(other->value);
			new_elt->color = other->color;
			new_elt->left = tree_deep_copy(other->left, other_nil);
			new_elt->right = tree_deep_copy(other->right, other_nil);
			new_elt->left->parent = new_elt;
			new_elt->right->parent = new_elt;
			return (new_elt);
		}

		node 	*insert_unique_at_position( node *new_node, node *position, bool *is_inserted ) {
			node	*cursor = position;
			node	*trailing_ptr = this->nil;
			while (cursor != this->nil) {
				trailing_ptr = cursor;
				if (!comp_(new_node->value, cursor->value) && !comp_(cursor->value, new_node->value)) {
					 *is_inserted = false;
					return (cursor);
				}
				else if (comp_(new_node->value, cursor->value))
					cursor = cursor->left;
				else
					cursor = cursor->right;
			}
			new_node->parent = trailing_ptr;
			if (trailing_ptr == this->nil)
				this->root_ = new_node;
			else if (comp_(new_node->value, trailing_ptr->value))
				trailing_ptr->left = new_node;
			else
				trailing_ptr->right = new_node;
			new_node->left = this->nil;
			new_node->right = this->nil;
			new_node->color = RED;
			RB_insert_fixup(new_node);
			*is_inserted = true;
			return (new_node);
		}

		void	left_rotate( node *x ) {
			node	*y = x->right;
			x->right = y->left;
			if (y->left != this->nil)
				y->left->parent = x;
			y->parent = x->parent;
			if (x->parent == this->nil)
				this->root_ = y;
			else if (x == x->parent->left)
				x->parent->left = y;
			else
				x->parent->right = y;
			y->left = x;
			x->parent = y;
		}

		void	right_rotate( node *y ) {
			node	*x = y->left;
			y->left = x->right;
			if (x->right != this->nil)
				x->right->parent = y;
			x->parent = y->parent;
			if (y->parent == this->nil)
				this->root_ = x;
			else if (y == y->parent->left)
				y->parent->left = x;
			else
				y->parent->right = x;
			x->right = y;
			y->parent = x;
		}

		void	RB_insert_fixup( node *n ) {
			while (n->parent->color == RED) {
				node	*y;
				if (n->parent == n->parent->parent->left) {
					y = n->parent->parent->right;
					if (y->color == RED) {
						n->parent->color = BLACK;
						y->color = BLACK;
						n->parent->parent->color = RED;
						n = n->parent->parent;
					}
					else {
						if (n == n->parent->right) {
							n = n->parent;
							left_rotate(n);
						}
						n->parent->color = BLACK;
						n->parent->parent->color = RED;
						right_rotate(n->parent->parent);
					}

				}
				else {
					// same as previous, with "right" and "left" swapped
					y = n->parent->parent->left;
					if (y->color == RED) {
						n->parent->color = BLACK;
						y->color = BLACK;
						n->parent->parent->color = RED;
						n = n->parent->parent;
					}
					else {
						if (n == n->parent->left) {
							n = n->parent;
							right_rotate(n);
						}
						n->parent->color = BLACK;
						n->parent->parent->color = RED;
						left_rotate(n->parent->parent);
					}
				}
			}
			this->root_->color = BLACK;
		}

		// DELETION

		void	RB_transplant( node *u, node *v ) {
			if (u->parent == this->nil)
				this->root_ = v;
			else if (u == u->parent->left)
				u->parent->left = v;
			else
				u->parent->right = v;
			v->parent = u->parent;
		}

		void	RB_delete( node *z ) {
			node *x;
			node *y = z;
			RBT_color y_original_color = y->color;
			if (z->left == this->nil) {
				x = z->right;
				RB_transplant(z, z->right);
			}
			else if (z->right == this->nil) {
				x = z->left;
				RB_transplant(z, z->left);
			}
			else {
				y = leftmost(z->right);
				y_original_color = y->color;
				x = y->right;
				if (y->parent == z)
					x->parent = y;
				else {
					RB_transplant(y, y->right);
					y->right = z->right;
					y->right->parent = y;
				}
				RB_transplant(z, y);
				y->left = z->left;
				y->left->parent = y;
				y->color = z->color;
			}
			if (y_original_color == BLACK)
				RB_delete_fixup(x);
		}

		void	RB_delete_fixup( node *x ) {
			while ( x != this->root_ && x->color == BLACK) {
				if (x == x->parent->left) {
					node *w = x->parent->right;
					if (w->color == RED) {
						w->color = BLACK;
						x->parent->color = RED;
						left_rotate(x->parent);
						w = x->parent->right;
					}
					if (w->left->color == BLACK && w->right->color == BLACK) {
						w->color = RED;
						x = x->parent;
					}
					else {
						if (w->right->color == BLACK) {
							w->left->color = BLACK;
							w->color = RED;
							right_rotate(w);
							w = x->parent->right;
						}
						w->color = x->parent->color;
						x->parent->color = BLACK;
						w->right->color = BLACK;
						left_rotate(x->parent);
						x = this->root_;
					}
				}
				else { // same as previous with "right" and "left" switched
					node *w = x->parent->left;
					if (w->color == RED) {
						w->color = BLACK;
						x->parent->color = RED;
						right_rotate(x->parent);
						w = x->parent->left;
					}
					if (w->right->color == BLACK && w->left->color == BLACK) {
						w->color = RED;
						x = x->parent;
					}
					else {
						if (w->left->color == BLACK) {
							w->right->color = BLACK;
							w->color = RED;
							left_rotate(w);
							w = x->parent->left;
						}
						w->color = x->parent->color;
						x->parent->color = BLACK;
						w->left->color = BLACK;
						right_rotate(x->parent);
						x = this->root_;
					}
				}
			}
			x->color = BLACK;
		}





	public:
		RBT( Compare vc, Alloc a) :  alloc_(a), comp_(vc), max(NULL), min(NULL), size_(0) {
		
			node*	sentinel = new_node(value_type());
			sentinel->color = BLACK;
			sentinel->right = this->max;
			sentinel->left = this->min;
			this->nil = sentinel;
			this->root_ = nil;
		} 

		RBT( const RBT<Key, T, TypeOfValue, Compare, Alloc>& x ) : alloc_(x.alloc_), comp_(x.comp_), size_(x.size_) {
			node *sentinel = new_node(x.nil->value);
			sentinel->color = BLACK;
			this->nil = sentinel;
			this->root_ = tree_deep_copy(x.root_, x.nil);
			this->max = rightmost(this->root_);
			this->min = leftmost(this->root_);
			sentinel->left = this->min; 
			sentinel->right = this->max; 
		}
	
		RBT&	operator=( const RBT<Key, T, TypeOfValue, Compare, Alloc>& rhs ) {
			this->clear();
			this->alloc_.destroy(this->nil);
			this->alloc_.deallocate(this->nil, 1);

			this->nil = new_node(rhs.nil->value);
			*(this->nil) = *(rhs.nil);
			this->root_ = tree_deep_copy(rhs.root_, rhs.nil);
			this->size_ = rhs.size_;
			return (*this);
		}

		~RBT() {
			this->clear();
			this->alloc_.destroy(this->nil);
			this->alloc_.deallocate(this->nil, 1);
		}

		node		*get_sentinel() const { return (this->nil); }

		size_type	get_size() const { return (this->size_); }

		Alloc		get_allocator() const { return (this->alloc_); }

		void	clear() {
			make_empty(this->root_);
			this->root_ = this->nil;
		}

		typedef	RBT_iterator	iterator;

		RBT_iterator	insert( const value_type& val, bool *is_inserted, iterator *position = NULL ) {
			node *position_node;
			if (!position)
				position_node = this->root_;
			else
				position_node = position->m_node;
			node	*new_elt = new_node(val);
			node	*inserted_node = insert_unique_at_position(new_elt, position_node, is_inserted);
			if (!(*is_inserted)) {
				this->alloc_.destroy(new_elt);
				this->alloc_.deallocate(new_elt, 1);
			}
			else {
				if (this->min == NULL || this->comp_(inserted_node->value, this->min->value))
					this->min = inserted_node;
				if (this->max == NULL || this->comp_(this->max->value, inserted_node->value))
					this->max = inserted_node;
				++this->size_;
			}
			RBT_iterator ret_it = RBT_iterator(inserted_node, this->nil);
			return (ret_it);
		}
		RBT_iterator	insert_with_hint( const value_type& val, iterator *hint) {
			bool is_inserted;
			iterator ret;
			if (this->comp_(val, hint->m_node->value) && (hint->m_node->right != nil && this->comp_(hint->m_node->right->value, val))) {
			 	ret = this->insert(val, &is_inserted, hint);
			}
			else 
				ret = this->insert(val, &is_inserted);
			return (ret);
		}

		void print_tree(const std::string& prefix, const node* node, bool isLeft) const {
			if ( node != this->nil ) {
				std::cout << prefix;

				std::cout << (isLeft ? "├L─" : "└R─" );

				// print the value of the node
				if (node->color == RED)
					std::cout << COLOR_RED;
				std::cout << node->value;
				std::cout << COLOR_RESET << std::endl;

				// enter the next tree level - left and right branch
				print_tree( prefix + (isLeft ? "│   " : "    "), node->left, true);
				print_tree( prefix + (isLeft ? "│   " : "    "), node->right, false);
			}
		}

		void print_tree() const {
			print_tree("", this->root_, false);    
			std::cout << "---- size: " << this->size_ << " ----\n";
		}

		void	*get_nil_address() const { return (this->nil); }
		node	*get_nil() const { return (this->nil); }

		node	*begin() const {
			this->nil->left = leftmost(this->root_);
			return (leftmost(this->root_));
		}

		node	*end() const {
			this->nil->right = rightmost(this->root_);
			return (this->nil);
		}

		void	erase( iterator position ) {
			node	*target = position.get_node();
			RB_delete(target);
			this->alloc_.destroy(target);
			this->alloc_.deallocate(target, 1);
			--this->size_;
		}

		iterator	find( const value_type& target ) const {
			node	*cursor = this->root_;
			while (cursor != this->nil) {
				if (this->comp_(target, cursor->value))
					cursor = cursor->left;
				else if (this->comp_(cursor->value, target))
					cursor = cursor->right;
				else
					return (iterator(cursor, this->nil));
			}
			return (iterator(end(), this->nil));
		}

		void	swap( RBT& other ) {
			
			node *tmp_root, *tmp_nil, *tmp_min, *tmp_max;
			size_type tmp_size;
			tmp_root = other.root_;
			tmp_nil = other.nil;
			tmp_min = other.min;
			tmp_max = other.max;
			tmp_size = other.size_;
			other.root_ = this->root_;
			other.nil = this->nil;
			other.min = this->min;
			other.max = this->max;
			other.size_ = this->size_;
			this->root_ = tmp_root;
			this->nil = tmp_nil;
			this->min = tmp_min;
			this->max = tmp_max;
			this->size_ = tmp_size;
		}

		iterator	lower_bound( const value_type& target ) const {
			if (this->root_ == this->nil)
				return (iterator(end(), this->nil));
			iterator	cursor = iterator(this->root_, this->nil);
			while (this->comp_(target, *cursor) && (cursor.m_node != leftmost(this->root_))) // k < node 
				--cursor;
			while (this->comp_(*cursor, target) && (cursor.m_node != this->nil) ) // node < k
				++cursor;
			return (cursor);	
		}

		iterator	upper_bound( const value_type& target ) const {
			iterator bound = lower_bound(target);
			if ( bound.m_node != this->nil) {
				while (!this->comp_(target, *bound) && (bound.m_node != this->nil))
					++bound;
			}
			return (bound);
		}
};


template <typename C>
std::ostream&	operator<<(std::ostream& os, const ft::RBT_node<C>& n) {
	os << "value: " << '<' << n.value.first << ',' << n.value.second << '>' << '\n';
	os << "left: " << n.left << '\n';
	os << "right: " << n.right << '\n';
	os << "parent: " << n.parent;
	return (os);
}

} // namespace ft

#endif
