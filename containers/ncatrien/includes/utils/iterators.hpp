#ifndef ITERATORS_HPP
#	define ITERATORS_HPP

#	include <iterator> // for std::iterator

/* Asked by subject: implement 
 *  - iterator_traits
 *  - reverse_iterator.
 *
 * 	A few considerations: 
 * - std::iterator has no functionality, contains only typedefs.
 * - iterator_traits is a template. 3 definitions: a fully generic one, one specialized for T*, and const T*
*/

namespace ft
{

template< class Iter >
struct iterator_traits
{
	typedef typename Iter::difference_type		difference_type;
	typedef typename Iter::value_type			value_type;
	typedef typename Iter::pointer				pointer;
	typedef typename Iter::reference			reference;
	typedef typename Iter::iterator_category	iterator_category;
};

template< class T >
struct iterator_traits<T*>
{
	typedef std::ptrdiff_t					difference_type;
	typedef T								value_type;
	typedef T*								pointer;
	typedef T&								reference;
	typedef std::random_access_iterator_tag	iterator_category;
};

template< class T >
struct iterator_traits<const T*>
{
	typedef std::ptrdiff_t					difference_type;
	typedef T								value_type;
	typedef const T*						pointer;
	typedef const T&						reference;
	typedef std::random_access_iterator_tag	iterator_category;
};

}

namespace ft
{

template< class Iter >
class reverse_iterator
{
	public:
		typedef Iter													iterator_type;
		typedef typename ft::iterator_traits<Iter>::iterator_category	iterator_category;
		typedef	typename ft::iterator_traits<Iter>::value_type			value_type;
		typedef	typename ft::iterator_traits<Iter>::difference_type		difference_type;
		typedef	typename ft::iterator_traits<Iter>::pointer				pointer;
		typedef	typename ft::iterator_traits<Iter>::reference			reference;

	protected:
		Iter itr;

	public:
		// CONSTRUCTORS
		//
		// by default
		reverse_iterator() {}
		// by initialization
		reverse_iterator( iterator_type it ) { this->itr = it; }
		// by copy
		template< class U >
		reverse_iterator( const reverse_iterator<U>& rev_it ) { this->itr = rev_it;}
		// end constructor //
		
		template< class U >
		reverse_iterator& operator=( const reverse_iterator<U>& other) {
			this->itr = other;
			return (*this);
		}

		iterator_type base() const {
			Iter copy = this->itr;
			return (--copy);
		}

		// OPERATORS
		reference	operator*() const {
			Iter copy_iter = itr;
			--copy_iter;
			return (*copy_iter);
		}

		reverse_iterator operator+( difference_type n) const {
			Iter copy_iter = itr;
			for (difference_type i = 0; i < n; ++i)
				copy_iter--;
			return (reverse_iterator(copy_iter));
		}

		reverse_iterator operator-( difference_type n) const {
			Iter copy_iter = itr;
			for (difference_type i = 0; i < n; ++i)
				copy_iter++;
			return (reverse_iterator(copy_iter));
		}

		// pre-increment
		reverse_iterator&	operator++() {
			this->itr--;
			return (*this);
		}
		// post-increment
		reverse_iterator	operator++(int) {
			reverse_iterator temp = *this;
			++(*this);
			return (temp);
		}
		// pre-decrement
		reverse_iterator&	operator--() {
			this->itr++;
			return (*this);
		}
		// post-decrement
		reverse_iterator	operator--(int) {
			reverse_iterator temp = *this;
			--(*this);
			return (temp);
		}
		
		reverse_iterator&	operator+=( difference_type n) {
			for (difference_type i = 0; i < n; ++i)
				this->itr--;
			return (*this);
		}
		
		reverse_iterator&	operator-=( difference_type n) {
			for (difference_type i = 0; i < n; ++i)
				this->itr++;
			return (*this);
		}
		
		pointer	operator->() const { return (&(operator*())); }

		reference operator[]( difference_type n ) const {
			return ( this->itr.base()[-n - 1] );
		}
};

// NON-MEMBER FUNCTION OVERLOADS
//
// relational operators
template <class Iterator1, class Iterator2>
bool operator==( const ft::reverse_iterator<Iterator1>& lhs,
				 const ft::reverse_iterator<Iterator2>& rhs) {
	return (lhs.base() == rhs.base());
}

template <class Iterator1, class Iterator2>
bool operator!=( const ft::reverse_iterator<Iterator1>& lhs,
				 const ft::reverse_iterator<Iterator2>& rhs) {
	return (lhs.base() != rhs.base());
}

template <class Iterator1, class Iterator2>
bool operator<( const ft::reverse_iterator<Iterator1>& lhs,
				 const ft::reverse_iterator<Iterator2>& rhs) {
	return (lhs.base() > rhs.base());
}

template <class Iterator1, class Iterator2>
bool operator<=( const ft::reverse_iterator<Iterator1>& lhs,
				 const ft::reverse_iterator<Iterator2>& rhs) {
	return (lhs.base() >= rhs.base());
}

template <class Iterator1, class Iterator2>
bool operator>( const ft::reverse_iterator<Iterator1>& lhs,
				 const ft::reverse_iterator<Iterator2>& rhs) {
	return (lhs.base() < rhs.base());
}

template <class Iterator1, class Iterator2>
bool operator>=( const ft::reverse_iterator<Iterator1>& lhs,
				 const ft::reverse_iterator<Iterator2>& rhs) {
	return (lhs.base() <= rhs.base());
}

template <class Iterator>
ft::reverse_iterator<Iterator> operator+( typename ft::reverse_iterator<Iterator>::difference_type n,
										const ft::reverse_iterator<Iterator>& rev_it ) {
	ft::reverse_iterator<Iterator> ret = rev_it;
	ret += n;
	return (ret);
}

template <class Iterator>
typename reverse_iterator<Iterator>::difference_type operator-( 
		const reverse_iterator<Iterator>& lhs,
		const reverse_iterator<Iterator>& rhs) {
	return (lhs.base() - rhs.base());
}

}
#endif
