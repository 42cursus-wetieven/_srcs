#ifndef ENABLE_IF_HPP
#	define ENABLE_IF_HPP

/*
 * Credits:
 * https://www.modernescpp.com/index.php/check-types
*/

namespace ft {

template <class T, T v>
struct integral_constant {
	const static T value = v;
	typedef T value_type;
	typedef integral_constant type;
	operator value_type() const { return (value); }
};

typedef integral_constant<bool, true> true_type;
typedef integral_constant<bool, false> false_type;

template <class T>
struct is_integral : public false_type{};

template<>
struct is_integral<bool> : public true_type{};

template<>
struct is_integral<char> : public true_type{};

template<>
struct is_integral<signed char> : public true_type{};

template<>
struct is_integral<unsigned char> : public true_type{};

template<>
struct is_integral<wchar_t> : public true_type{};

template<>
struct is_integral<short> : public true_type{};

template<>
struct is_integral<int> : public true_type{};

template<>
struct is_integral<long> : public true_type{};

template<>
struct is_integral<long long> : public true_type{};

template<>
struct is_integral<unsigned short> : public true_type{};

template<>
struct is_integral<unsigned int> : public true_type{};

template<>
struct is_integral<unsigned long> : public true_type{};

template<>
struct is_integral<unsigned long long> : public true_type{};



template <bool B, class T = void>
struct enable_if {
};

template <class T>
struct enable_if<true, T> {
	typedef T type;
};

}

#endif
