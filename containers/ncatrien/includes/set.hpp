#ifndef SET_HPP
#	define SET_HPP

#	include <functional> // std::less
#	include <memory> 	// std::allocator
#	include <cstddef>	// std::ptrdiff_t
#	include "trees.hpp"
#	include "pair.hpp"
#	include "lexicographical_compare.hpp"


namespace ft
{

template <	class T,
		 	class Compare = std::less<T>,
		 	class Alloc = std::allocator<T>
		>
class set {

	public:
		typedef	T											key_type;
		typedef T											value_type;
		typedef Compare										key_compare;
		typedef Compare 									value_compare;
		typedef Alloc										allocator_type;
		typedef typename allocator_type::reference			reference;
		typedef typename allocator_type::const_reference	const_reference;
		typedef typename allocator_type::pointer			pointer;
		typedef typename allocator_type::const_pointer		const_pointer;
		//iterator
		//const_iterator
		//reverse_iterator
		//const_reverse_iterator
		typedef ptrdiff_t									difference_type;
		typedef size_t										size_type;

	private:
		typedef typename allocator_type::template
			rebind<RBT_node<value_type> >::other			node_allocator;

		typedef RBT<key_type, value_type, value_type, value_compare, node_allocator> Rep_type;

	public:
		typedef typename Rep_type::RBT_iterator				iterator;
		typedef typename Rep_type::const_RBT_iterator		const_iterator;
		typedef ft::reverse_iterator<iterator>				reverse_iterator;
		typedef ft::reverse_iterator<const_iterator>		const_reverse_iterator;


		// CONSTRUCTOR/DESTRUCTOR/ASSIGNATION OPERATOR

		explicit set( const key_compare& comp = key_compare(),
						const allocator_type& alloc = allocator_type() ) :
			allocator_(alloc), key_comp_(comp), tree_(key_comp_, alloc) {}

		template <class InputIterator>
		set ( InputIterator first, InputIterator last,
				const key_compare& comp = key_compare(),
				const allocator_type& alloc = allocator_type() ) :
			allocator_(alloc), key_comp_(comp), tree_(key_comp_, alloc) {
				while (first != last) {
					insert(*first);
					++first;
				}
		}

		set ( const set& x ) : allocator_(x.allocator_), key_comp_(x.key_comp_), tree_(x.tree_) {}

		~set() {}

		set&	operator=( const set& x ) {
			this->tree_.clear();
			this->tree_ = x.tree_;
			return (*this);
		}

		// ITERATORS

		iterator				begin() {
			return (iterator(this->tree_.begin(), this->tree_.get_sentinel()));
		}
		const_iterator			begin() const {
			return (const_iterator(this->tree_.begin(), this->tree_.get_sentinel()));
		}
		
		iterator				end() {
			return (iterator(this->tree_.end(), this->tree_.get_sentinel()));
		}
		const_iterator 			end() const {
			return (const_iterator(this->tree_.end(), this->tree_.get_sentinel()));
		}

		reverse_iterator 		rbegin() {
			return (reverse_iterator(this->end()));
		}
		const_reverse_iterator	rbegin() const {
			return (const_reverse_iterator(this->end()));
		}

		reverse_iterator 		rend() {
			return (reverse_iterator(this->begin()));
		}
		const_reverse_iterator	rend() const {
			return (const_reverse_iterator(this->begin()));
		}

		// CAPACITY
		
		bool		empty() const { return (size() == 0);}

		size_type	size() const { return (this->tree_.get_size()); }

		size_type	max_size() const {
			return ( std::numeric_limits<size_type>::max() / sizeof(RBT_node<value_type>) );
		}

		// MODIFIERS
		//	
		// Insert
		ft::pair<iterator, bool>	insert( const value_type& val) {
			bool is_inserted;
			iterator it_insert = this->tree_.insert(val, &is_inserted);
			ft::pair<iterator, bool> ret_pair = ft::make_pair(it_insert, is_inserted);
			return (ret_pair);
		}
		iterator					insert( iterator position, const value_type& val ) {
			iterator it = this->tree_.insert_with_hint(val, &position);
			return (it);
		}
		template <class InputIterator>
		void						insert(InputIterator first, InputIterator last) {
			bool is_inserted;
			while (first != last) 
				this->tree_.insert(*first++, &is_inserted);
		}

		void						erase( iterator position ) {
			this->tree_.erase(position);
		}
		size_type					erase( const key_type& k ) {
			iterator target = find(k);
			if (target == this->end())
				return (0);
			erase(target);
			return (1);
		}
		void						erase( iterator first, iterator last) {
			iterator tmp;
			while (first != last) {
				tmp = first;
				++first;
				erase(tmp);
			}
		}

		void						swap( set& x ) {
			this->tree_.swap(x.tree_);
		}

		void						clear() { this->tree_.clear(); }
			
		// OBSERVERS

		key_compare		key_comp() const { return (this->key_comp_); }
		value_compare	value_comp() const { return (this->key_comp_); }

		// OPERATIONS

		iterator		find( const key_type& k ) {
			return (this->tree_.find(k));
		}
		const_iterator	find( const key_type& k ) const {
			return (this->tree_.find(k));
		}

		size_type		count( const key_type& k ) const {
			return (this->tree_.find(k) == this->end() ? 0 : 1);
		}

		iterator		lower_bound( const key_type& k ) {
			return (this->tree_.lower_bound(k));
		}
		const_iterator	lower_bound( const key_type& k ) const {
			return (this->tree_.lower_bound(k));
		}
		
		iterator		upper_bound( const key_type& k ) {
			return (this->tree_.upper_bound(k));
		}
		const_iterator	upper_bound( const key_type& k ) const {
			return (this->tree_.upper_bound(k));
		}

		ft::pair<iterator, iterator>				equal_range( const key_type& k ) {
			return (ft::pair<iterator, iterator>(lower_bound(k), upper_bound(k)));
		}
		ft::pair<const_iterator, const_iterator>	equal_range( const key_type& k ) const {
			return (ft::pair<const_iterator, const_iterator>(lower_bound(k), upper_bound(k)));
		}

		// ALLOCATOR

		allocator_type	get_allocator() const { return (this->allocator_); }

		// showing inner red black tree
		void	print_tree() const { this->tree_.print_tree(); }

	private:
		allocator_type	allocator_;
		key_compare		key_comp_;
		Rep_type		tree_;


}; //set

// NON-MEMBER OVERLOADS

// relational operators

template< class T, class Compare, class Alloc >
bool	operator==( const set<T, Compare, Alloc> lhs,
					const set<T, Compare, Alloc> rhs ) {
	typedef typename  set<T, Compare, Alloc>::const_iterator const_it;
	if ( lhs.size() == rhs.size() ){
		const_it rhs_it = rhs.begin();
		const_it lhs_it = lhs.begin();
		while (lhs_it != lhs.end()) {
			if (*lhs_it != *rhs_it)
				return (false);
			++lhs_it;
			++rhs_it;
		}
		return (true);
	}
	else
		return (false);
}

template< class T, class Compare, class Alloc >
bool	operator!=( const set<T, Compare, Alloc> lhs,
					const set<T, Compare, Alloc> rhs ) {
	return (!(lhs == rhs));
}

template< class T, class Compare, class Alloc >
bool	operator<( const set<T, Compare, Alloc> lhs,
					const set<T, Compare, Alloc> rhs ) {
	return (ft::lexicographical_compare(lhs.begin(), lhs.end(),
										rhs.begin(), rhs.end()));
}

template< class T, class Compare, class Alloc >
bool	operator<=( const set<T, Compare, Alloc> lhs,
					const set<T, Compare, Alloc> rhs ) {
	return ( !(rhs < lhs) );
}

template< class T, class Compare, class Alloc >
bool	operator>( const set<T, Compare, Alloc> lhs,
					const set<T, Compare, Alloc> rhs ) {
	return ( rhs < lhs );
}

template< class T, class Compare, class Alloc >
bool	operator>=( const set<T, Compare, Alloc> lhs,
					const set<T, Compare, Alloc> rhs ) {
	return ( !(lhs < rhs) );
}

// swap

template < class T, class Compare, class Alloc >
void	swap( set<T, Compare, Alloc>&x, set<T, Compare, Alloc>& y ) {
	x.swap(y);
}

} // namespace ft

#endif
