#ifndef VECTOR_HPP
#	define VECTOR_HPP

#	include <memory>	// for std::allocator
#	include <iostream>	// debug
#	include <stdexcept>
#	include <limits>	// for max_size()
#	include <algorithm> // for std::copy, std::equal
#	include "iterators.hpp"
#	include "enable_if.hpp"
#	include "lexicographical_compare.hpp"

namespace ft
{

template < typename T, typename Allocator = std::allocator<T> >
class vector
{
	public:
		class vector_iterator; 
		class const_vector_iterator; 
		typedef T										value_type;
		typedef Allocator								allocator_type;
		typedef std::size_t								size_type;
		typedef std::ptrdiff_t							difference_type;
		typedef value_type&								reference;
		typedef const value_type&						const_reference;
		typedef	typename Allocator::pointer				pointer; 		
		typedef typename Allocator::const_pointer		const_pointer;	
		typedef vector_iterator							iterator;
		typedef const_vector_iterator					const_iterator; 
		typedef ft::reverse_iterator<iterator>			reverse_iterator;
		typedef	ft::reverse_iterator<const_iterator>	const_reverse_iterator;

		class vector_iterator : public std::iterator<std::random_access_iterator_tag, value_type> {
		// source:
		// https://cs.stanford.edu/people/eroberts/courses/cs106b/materials/cppdoc/vector-h.html
			protected:
				const vector	*p;
				size_type		index;
			public:
				vector_iterator() { this->p = NULL; }
				vector_iterator(const iterator& it) : p(it.p), index(it.index) {}
				vector_iterator(const vector *vp, int index) : p(vp), index(index) {}
				vector_iterator&	operator=(vector_iterator const& rhs) {
					this->p = rhs.p;
					this->index = rhs.index;
					return (*this);
				}
				vector_iterator&	operator++() {
					this->index++;
					return (*this);
				}
				vector_iterator		operator++(int) {
					vector_iterator copy(*this);
					operator++();
					return (copy);
				}
				vector_iterator&	operator--() {
					this->index--;
					return (*this);
				}
				vector_iterator		operator--(int) {
					vector_iterator	copy(*this);
					operator--();
					return (*this);
				}
				bool				operator==( const vector_iterator& rhs ) const {
					return (this->p == rhs.p && this->index == rhs.index);
				}
				bool				operator!=( const vector_iterator& rhs ) const {
					return (!(*this == rhs));
				}
				// Remark:
				// std::vector::iterator doesn't throw if comparing iterators
				// from different vectors.
				bool				operator<( const vector_iterator& rhs ) const {
					return (this->index < rhs.index);
				}
				bool				operator<=( const vector_iterator& rhs ) const {
					return (this->index <= rhs.index);
				}
				bool				operator>( const vector_iterator& rhs ) const {
					return (this->index > rhs.index);
				}
				bool				operator>=( const vector_iterator& rhs ) const {
					return (this->index >= rhs.index);
				}
				vector_iterator		operator+( const int& rhs ) const {
					return (vector_iterator(this->p, this->index + rhs));
				}
				vector_iterator		operator+=( const int& rhs) {
					this->index += rhs;
					return (*this);
				}
				vector_iterator		operator-=( const int& rhs) {
					this->index -= rhs;
					return (*this);
				}
				vector_iterator		operator-( const int& rhs ) const {
					return (vector_iterator(this->p, this->index - rhs));
				}
				int					operator-( const vector_iterator& rhs ) const {
					return (this->index - rhs.index);
				}
				value_type&			operator*() {
					return (p->data_[this->index]);
				}
				value_type&			operator->() {
					return (&p->data_[this->index]);
				}
				value_type&			operator[]( int k ) {
					return (p->data_[index + k]);
				}
		};

		class const_vector_iterator : public vector_iterator {

			public:
				typedef const value_type&						reference;
			public:
				const_vector_iterator() : vector_iterator() {}
				const_vector_iterator(const iterator& it) : vector_iterator(it) {}
				const_vector_iterator(const vector *vp, int index) : vector_iterator(vp, index) {}
				const_vector_iterator&	operator=( const_vector_iterator const& rhs ) {
					vector_iterator::operator=(rhs);
					return (*this);
				}

				value_type const &	operator*() const {
					return (this->p->data_[this->index]);
				}
				value_type const &			operator->() {
					return (&this->p->data_[this->index]);
				}
		};



	private:
		size_type		size_;
		size_type		capacity_;
		value_type*		data_;
		allocator_type	allocator_;

		void			expandCapacity() {
			size_type	new_capacity = std::max(static_cast<size_type>(1), this->capacity_ * 2);
			value_type	*tmp_array = this->allocator_.allocate(new_capacity);
			deepCopy(tmp_array, this->data_, this->size_);
			for (size_type i = 0; i < this->size_; ++i)
				this->allocator_.destroy(this->data_ + i);
			this->allocator_.deallocate(this->data_, this->capacity_);
			this->capacity_ = new_capacity;
			this->data_ = tmp_array;
		}

		void			deepCopy( value_type* dst, value_type* src, size_type n ) {
			for (size_type i = 0; i < n; ++i)
				this->allocator_.construct(dst + i, src[i]);
		}

	public:
		// CONSTRUCTORS
		//
		// default
		explicit vector( const allocator_type& alloc = allocator_type()) :
			size_(0), capacity_(0), data_(NULL), allocator_(alloc) {}
		// fill
		explicit vector( size_type n, const value_type& val = value_type(), const allocator_type& alloc = allocator_type() ) {
			this->size_ = n;
			this->capacity_ = n;
			allocator_= alloc;
			data_= this->allocator_.allocate(n);
			for (size_t i = 0; i < n; ++i)
				this->allocator_.construct(this->data_ + i, val);
		}
		// range 
		template <class InputIterator>
		vector ( InputIterator first, InputIterator last, const allocator_type& alloc = allocator_type(),
			 typename ft::enable_if<!ft::is_integral<InputIterator>::value, InputIterator>::type* = NULL ) {
			difference_type range_size = last - first;
			this->allocator_= alloc;
			this->size_ = range_size;
			this->capacity_ = range_size;
			data_= this->allocator_.allocate(range_size);
			std::size_t i = 0;
			while (first != last) {
				this->allocator_.construct(this->data_ + i, *first);
				++first;
				++i;
			}
		}
		// copy
		vector (const vector& x) {
			this->capacity_ = x.capacity();
			this->size_ = x.size();
			this->allocator_= x.get_allocator();
			this->data_ = this->allocator_.allocate(this->capacity_);
			deepCopy(this->data_, x.data_, this->size_);
		}

		// DESTRUCTOR
		~vector() {
			if (this->data_ != NULL) {
				this->clear();
				this->allocator_.deallocate(this->data_, this->capacity_);
			}
		}

		// copy assignment
		vector& operator=( const vector& other ) {
			if (this->data_ != NULL) {
				for (size_type i = 0; i < this->size_; ++i)
					this->allocator_.destroy(this->data_ + i);
				this->allocator_.deallocate(this->data_, this->capacity_);
			}
			this->size_ = other.size();
			this->capacity_ = other.capacity();
			this->data_ = this->allocator_.allocate(this->capacity_);
			deepCopy(this->data_, other.data_, this->size_);
			return (*this);
		}

		allocator_type	get_allocator() const { return( this->allocator_);}

		// ELEMENT ACCESS
		//
		reference 		at( size_type pos ) {
			if (pos >= this->size())
				throw std::out_of_range("out of range exception");
			return (this->data_[pos]);
		}

		reference		operator[]( size_type pos ) { return (this->data_[pos]); }
		const_reference operator[]( size_type pos ) const { return (this->data_[pos]); }

		reference 		front() { return (this->data_[0]); }
		const_reference front() const { return (this->data_[0]); }

		reference 		back() { return (this->data_[this->size_ - 1]); }
		const_reference back() const { return (this->data_[this->size_ - 1]); }

		value_type 		*data() { return (&this->data_[0]); }


		// CAPACITY
		//
		size_type 	capacity() const { return (this->capacity_);}

		size_type 	size() const { return (this->size_);}

		size_type 	max_size() const { return ( 2 * std::numeric_limits<difference_type>::max() / sizeof(value_type)); }

		void		reserve( size_type new_cap ) {
			if (new_cap > max_size())
				throw std::length_error("new_capacity is greater than max size");
			while (new_cap < this->capacity_)
				expandCapacity();
		}

		bool		empty() const { return (this->size_ == 0); }

		void		resize( size_type n, value_type val = value_type() ) {
			if (n < this->size_) {
				while (this->size_ != n)
					this->allocator_.destroy(this->data_ + this->size_-- - 1);
			}
			else if (n > this->size_)
				insert( this->end(), n - this->size_, val );
		}

		// ITERATORS
		//
		iterator				begin() {
			return (vector_iterator(this, 0));
		}
		const_iterator			begin() const {
			return (const_vector_iterator(this, 0));
		}

		iterator				end() { return (vector_iterator(this, this->size_)); }
		const_iterator			end() const { return (const_vector_iterator(this, this->size_)); }

		reverse_iterator		rbegin() { return (reverse_iterator(this->end())); }
		const_reverse_iterator	rbegin() const {
			return (const_reverse_iterator(this->end()));
		}

		reverse_iterator		rend() { return (reverse_iterator(this->begin())); }
		const_reverse_iterator	rend() const { return (const_reverse_iterator(this->begin())); }

		// MODIFIERS
		//
		void		clear() {
			for (size_type i = 0; i < this->size_; ++i)
				this->allocator_.destroy(this->data_ + i);
			this->size_ = 0;
		}
		// insert
		iterator	insert( iterator pos, const value_type& val ) {
			if (this->size_ == this->capacity_)
				expandCapacity();
			for ( iterator it = this->end(); it != pos; --it)
				*it = *(it - 1);
			this->allocator_.construct(this->data_ + (pos - begin()), val);
			this->size_++;
			return (pos);
		}
		void		insert( iterator position, size_type n, const value_type& val ) {
			for (size_type i = 0; i < n; ++i)
				position = this->insert(position, val);
		}
		template <class InputIterator>
		void		insert( iterator position, InputIterator first, InputIterator last,
				 			typename ft::enable_if<!ft::is_integral<InputIterator>::value, InputIterator>::type* = NULL ) {
			while (first != last) {
				const value_type& value = *first;
				this->insert(position, value);
				first++;
			}
		}
		// assign
		template <class InputIterator>
		void	assign( InputIterator first, InputIterator last,
				 		typename ft::enable_if<!ft::is_integral<InputIterator>::value, InputIterator>::type* = NULL ) {
			this->clear();
			while (first != last) {
				this->insert(this->end(), *first);
				first++;
			}
		}	
		void	assign( size_type n, const value_type& val) {
			this->clear();
			this->insert(this->begin(), n, val);
		}

		// push_back
		void	push_back( const value_type& val) {
			insert(this->end(), val);
		}
		void	pop_back() {
			if (this->size_) {
				--(this->size_);
				this->allocator_.destroy(this->data_ + this->size_);
			}
		}
		// erase
		//
		iterator	erase(iterator position) {
			difference_type diff_from_last = (this->end() - 1) - position;
			this->allocator_.destroy(this->data_ + (this->size_ - 1 - diff_from_last));
			std::copy(position + 1, this->end(), position);
			this->allocator_.destroy(this->data_ + --(this->size_)); // delete last elt in double
			return (iterator(this, this->size_ - diff_from_last));
		}
		iterator	erase(iterator first, iterator last) {
			difference_type nb_to_erase = last - first;
			std::copy(last, this->end(), first);
			this->resize(this->size_ - nb_to_erase);
			return (first);
		}

		void		swap( vector& x ) {
			value_type	*tmp_data = this->data_;
			size_type		tmp_size = this->size_;
			size_type		tmp_capacity = this->capacity_;
			this->data_ = x.data_;
			this->size_ = x.size();
			this->capacity_ = x.capacity();
			x.data_ = tmp_data;
			x.size_ = tmp_size;
			x.capacity_ = tmp_capacity;
		}

};

// NON-MEMBER FUNCTION OVERLOADS
//
// relational operators

template <class T, class Alloc>
bool	operator==( const vector<T, Alloc>& lhs, const vector<T, Alloc>& rhs ) {
	if (rhs.size() != lhs.size()) 
		return (false);
	return (std::equal(lhs.begin(), lhs.end(), rhs.begin()));
}

template <class T, class Alloc>
bool	operator!=( const vector<T, Alloc>& lhs, const vector<T, Alloc>& rhs ) {
	return (!(lhs == rhs));
}

template <class T, class Alloc>
bool	operator<( const vector<T, Alloc>& lhs, const vector<T, Alloc>& rhs) {

	return (ft::lexicographical_compare(lhs.begin(), lhs.end(),
										rhs.begin(), rhs.end()));
}

template <class T, class Alloc>
bool	operator<=( const vector<T, Alloc>& lhs, const vector<T, Alloc>& rhs) {
	return (! (rhs < lhs));
}

template <class T, class Alloc>
bool	operator>( const vector<T, Alloc>& lhs, const vector<T, Alloc>& rhs) {
	return ((rhs < lhs));
}

template <class T, class Alloc>
bool	operator>=( const vector<T, Alloc>& lhs, const vector<T, Alloc>& rhs) {
	return (!(lhs < rhs));
}

// swap overload
template <class T, class Alloc>
void	swap( vector<T, Alloc>& x, vector<T, Alloc>& y) {
	x.swap(y);
}

}
#endif
