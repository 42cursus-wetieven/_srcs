/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   User.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/08 14:36:18 by chervy            #+#    #+#             */
/*   Updated: 2021/10/26 09:44:49 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <User.hpp>

User::User()
{
	this->_sockfd = -1;
	this->_buffer.clear();
	this->_bufferOverflow = 0;
	this->_host.clear();
	this->_nickname = "*";
	this->_username = "*";
	this->_realname = "*";
	this->_connected = 1;
	this->_register = 0;
	this->_oper = 0;
	this->_auth = 0;
	this->_chans.clear();
}

User::User(User const &user)
{
	this->operator=(user);
}

User::~User() {}

User &		User::operator=(User const &rhs)
{
	this->_sockfd = rhs._sockfd;
	this->_buffer = rhs._buffer;
	this->_bufferOverflow = 0;
	this->_host	= rhs._host;
	this->_nickname = rhs._nickname;
	this->_username = rhs._username;
	this->_realname = rhs._realname;
	this->_connected = rhs._connected;
	this->_register = rhs._register;
	this->_oper = rhs._oper;
	this->_auth = rhs._auth;
	this->_chans = rhs._chans;
	return (*this);
}

int	User::operator==(User const & rhs) const
{
	return (this->_sockfd == rhs._sockfd);
}

/*
** Get
*/

int	User::getSockfd() const
{
	return (this->_sockfd);
}

std::string const &	User::getBuffer() const
{
	return (this->_buffer);
}

std::string const &	User::getHost() const
{
	return _host;
}

std::string	User::getNickname() const
{
	return (this->_nickname);
}

std::string	User::getUsername() const
{
	return (this->_username);
}

std::string	User::getRealname() const
{
	return (this->_realname);
}

std::list<Channel *> const& User::getChans(void) const
{
	return _chans;
}

/*
** Is
*/

int User::isConnected() const
{
	return _connected;
}

int	User::isRegister()
{
	if (this->_nickname != "*" && this->_username != "*" && this->_sockfd != -1 && isAuth())
		this->_register = 1;
	return (this->_register);
}

int	User::isOper() const
{
	return _oper;
}

int User::isAuth() const
{
	return _auth;
}

/*
** Set
*/

void	User::setSockfd(int const & sockfd)
{
	this->_sockfd = sockfd;
}

void	User::setHost(std::string const & host)
{
	_host = host;
}

void	User::setNickname(std::string const & nickname)
{
	this->_nickname = nickname;
}

void	User::setUsername(std::string const & username)
{
	this->_username = username;
}

void	User::setRealname(std::string const & realname)
{
	this->_realname = realname;
}

void User::setOper(int is) 
{
	_oper = is;
}

void User::setAuth(int is) 
{
	_auth = is;
}

/*
** Buffer
*/

void	User::upBuffer(const char * buffer)
{
	size_t i = this->_buffer.find_first_of("\n");
	if (i == std::string::npos && this->_buffer.size() >= 512)
	{
		this->_bufferOverflow = 1;
		this->_buffer.clear();
	}
	this->_buffer += buffer;
	if (_bufferOverflow)
	{
		size_t i = this->_buffer.find_first_of("\n");
		if (i != std::string::npos)
		{
			_buffer.erase(_buffer.begin(), _buffer.begin() + i + 1);
			_bufferOverflow = 0;
		}
	}
}

int	User::isReadyBuffer()
{
	size_t i = _buffer.find_first_of("\n");

	return (i != std::string::npos);
}

std::string User::getReadyBuffer() 
{
	size_t i = _buffer.find_first_of("\n");

	std::string ret = _buffer.substr(0, i);
	_buffer.erase(_buffer.begin(), _buffer.begin() + i + 1);

	return ret;
}

void	User::clearBuffer()
{
	this->_buffer.clear();
}

/*
** Channels
*/

int User::addChan(Channel * chan) 
{
	for (std::list<Channel *>::iterator it = _chans.begin(); it != _chans.end(); it++)
	{
		if (*it == chan)
		{
			return 0;
		}
	}
	_chans.push_back(chan);
	return 1;
}

void User::delChan(Channel * chan) 
{
	for (std::list<Channel *>::iterator it = _chans.begin(); it != _chans.end(); it++)
	{
		if (*it == chan)
		{
			_chans.erase(it);
			return ;
		}
	}
}

/*
** Other
*/

void User::deconnect() 
{
	_connected = 0;
}

