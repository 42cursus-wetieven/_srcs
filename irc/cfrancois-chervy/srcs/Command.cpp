/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Command.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/08 17:26:16 by chervy            #+#    #+#             */
/*   Updated: 2021/10/24 18:08:55 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <Command.hpp>
#include <Message.hpp>


const size_t	Command::_cmdPairSize = 13;

Command::cmdPair_type	Command::_cmdPair[Command::_cmdPairSize] = {
	std::make_pair("PASS", Command::cmdPass),
	std::make_pair("NICK", Command::cmdNick),
	std::make_pair("USER", Command::cmdUser),
	std::make_pair("QUIT", Command::cmdQuit),
	std::make_pair("OPER", Command::cmdOper),
	std::make_pair("JOIN", Command::cmdJoin),
	std::make_pair("PART", Command::cmdPart),
	std::make_pair("KICK", Command::cmdKick),
	std::make_pair("PRIVMSG",Command::cmdPrivmsg),
	std::make_pair("DIE",Command::cmdDie),
	std::make_pair("NOTICE",Command::cmdNotice),
	std::make_pair("KILL",Command::cmdKill),
	std::make_pair("PING",Command::cmdPing),
};

Command::Command(Command const &command) : _user(command._user)
{
	this->_command = command._command;
	return;
}

Command::Command(User & user) : _user(user)
{
	std::string	buffer = user.getReadyBuffer();
	size_t		sizeBuffer = buffer.find_first_of(13);

	if (sizeBuffer == std::string::npos)
		sizeBuffer = buffer.find_first_of(10);
	parsing(buffer.substr(0, sizeBuffer));
}

Command::~Command() {}

Command &	Command::operator=(Command const & rhs)
{
	this->_command = rhs._command;
	return (*this);
}

User const &	Command::getUser(void) const
{
	return (this->_user);
}

std::list<std::string> const & Command::getCommand() const
{
	return (this->_command);
}

std::string	Command::getParam(size_t n) const
{
	command_type::const_iterator	it;
	command_type::const_iterator	end = this->_command.end();
	size_t							i = 0;
	
	for (it = this->_command.begin(); it != end; it++)
	{
		if (i == n)
		{
			return (*it);
		}
		i++;
	}
	return ("");
}

void	Command::setCommand(std::list<std::string> const & ref) 
{
	_command = ref;
}

void	Command::exec(Server & server)
{
	size_t		idx = 0;
	std::string	tmp = ft_toUpper(this->_command.front());

	while (idx < _cmdPairSize && _cmdPair[idx].first != tmp)
	{
		idx++;
	}

	if (idx < _cmdPairSize && _cmdPair[idx].first == tmp)
	{
		log();
		if (idx > 3 && !this->_user.isRegister())
		{
			Message msg(*this, server);
			msg.send(this->_user.getSockfd(), ERR_NOTREGISTERED);
		}
		else
		{
			(_cmdPair[idx].second)(*this, server);
		}
	}
}

void	Command::parsing(std::string buffer)
{
	size_t		space;
	std::string	tmp;

	if (!buffer.empty() && buffer[0] == ':')
	{
		space = buffer.find_first_of(' ');
		if (space != std::string::npos)
			buffer.erase(0, space + 1);
		else
			buffer.erase(space);
	}

	space = buffer.find_first_of(' ');
	while (space != std::string::npos && buffer[0] != ':')
	{
		if (space != 0)
			this->_command.push_back(buffer.substr(0, space));
		buffer.erase(0, space + 1);
		space = buffer.find_first_of(' ');
	}
	if (!buffer.empty() && buffer[0] == ':')
	{
		this->_command.push_back(buffer.substr(1));
		buffer.clear();
	}
	else if (!buffer.empty() && space == std::string::npos)
	{
		if (space != 0)
			this->_command.push_back(buffer);
		buffer.clear();
	}
}

void	Command::log()
{
	std::cout << "---------------------------------------------------------" << std::endl;
	std::cout  << "Command	: ";
	for (std::list<std::string>::iterator it = this->_command.begin(); it != this->_command.end(); it++)
	{
		std::cout  << "<" << *it << "> ";
	}
	std::cout << std::endl;
	std::cout << "---------------------------------------------------------" << std::endl;
}
