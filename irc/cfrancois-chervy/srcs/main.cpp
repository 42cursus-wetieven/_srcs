/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/25 19:22:10 by chervy            #+#    #+#             */
/*   Updated: 2021/10/26 10:15:33 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <ircserv.hpp>
#include <Server.hpp>
#include <User.hpp>
#include <Channel.hpp>
#include <Command.hpp>
#include <Message.hpp>

int	ft_isPort(std::string str)
{
	for (int i = 0; str[i]; i++)
	{
		if (ft_isDigit(str[i]) == 0)
			return (-1);
	}

	int x = ft_atoi(str);
	if (x < 1 || x > 65535)
		return (-1);

	return (x);
}

int main(int argc, char **argv)
{
	if (argc != 3)
	{
		std::cout << "Bad arguments." << std::endl;
		std::cout << "usage : ./ircserv <port> <password>" << std::endl;
		return (0);
	}

	int port = ft_isPort(argv[1]);
	if (port == -1)
	{
		std::cout << "Bad port." << std::endl;
		std::cout << "usage : ./ircserv <port> <password>" << std::endl;
		return (0);
	}


	std::string	password(argv[2]);
	if (password.size() > 512 - 5)
	{
		std::cout << "Bad password." << std::endl;
		std::cout << "usage : ./ircserv <port> <password>" << std::endl;
		return (0);
	}
	
	Server	irc(port, argv[2]);

	return (irc.run());
}
