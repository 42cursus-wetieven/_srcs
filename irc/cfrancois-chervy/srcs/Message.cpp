/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Message.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/14 13:53:48 by chervy            #+#    #+#             */
/*   Updated: 2021/10/26 12:10:45 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <Message.hpp>

const size_t	Message::_msgMapSize = 24;

Message::msgPair_type	Message::_msgPair[Message::_msgMapSize] = {
	std::make_pair(1, Message::msgWelcome),
	std::make_pair(2, Message::msgHost),
	std::make_pair(251, Message::msgNbrUsers),
	std::make_pair(353, Message::msgNameReply),
	std::make_pair(366, Message::msgEndOfNames),
	std::make_pair(401, Message::msgNoSuchNick),
	std::make_pair(403, Message::msgNoSuchChannel),
	std::make_pair(404, Message::msgCannotSendToChan),
	std::make_pair(405, Message::msgTooManyChannels),
	std::make_pair(411, Message::msgNoRecipient),
	std::make_pair(412, Message::msgNoTextToSend),
	std::make_pair(431, Message::msgNoNicknameGiven),
	std::make_pair(432, Message::msgErroneusNickname),
	std::make_pair(433, Message::msgNicknameInUse),
	std::make_pair(436, Message::msgNickCollision),
	std::make_pair(442, Message::msgNotOnChannel),
	std::make_pair(451, Message::msgNotRegistered),
	std::make_pair(461, Message::msgNeedMoreParams),
	std::make_pair(464, Message::msgPasswdMismatch),
	std::make_pair(462, Message::msgAlreadyRegistred),
	std::make_pair(475, Message::msgBadChannelKey),
	std::make_pair(476, Message::msgBadChanMask),
	std::make_pair(481, Message::msgNoPrivileges),
	std::make_pair(482, Message::msgChanPrivsNeeded)
};

Message::msgMap_type	Message::_msgMap(Message::_msgPair, Message::_msgPair + Message::_msgMapSize);


Message::Message(Message const &message) : _server(message._server), _cmd(message._cmd)
{
	this->_msg = message._msg;
}

Message::Message(Command const & cmd, Server const & server) : _server(server), _cmd(cmd)
{
	this->_msg.clear();
}

Message::~Message(void) {}

void	Message::up(std::string msg, int prefix)
{
	const User &	user = this->_cmd.getUser();
	switch (prefix)
	{
		case 0:	// MSG
		{
			this->_msg = msg + "\n";
			break;
		}
		case 1: // RPL
		{
			this->_msg = ":" + user.getNickname() + "!~" + user.getUsername();
			this->_msg += "@" + user.getHost() + " ";
			this->_msg += msg;
			this->_msg += "\n";
			break;
		}
		case 2: // NOTICE
		{
			this->_msg = ":" + this->_server.getHost() + " NOTICE ";
			this->_msg += user.getNickname() + " ";
			this->_msg += ":*** " + msg;
			this->_msg += "\n";
			break;
		}
		default:
			break;
	}
}

void	Message::up(int idx)
{
	this->_msg = _msgMap[idx](this->_cmd, this->_server);
}

int	Message::send(int sockfd)
{
	std::cout << "---------------------------------------------------------" << std::endl;
	std::cout << "Message to fd " << sockfd << "." << std::endl;
	std::cout << this->_msg;
	std::cout << "---------------------------------------------------------" << std::endl;
	
	return (::send(sockfd, this->_msg.c_str(), this->_msg.size(), 0));
}

int	Message::send(int sockfd, int idx)
{
	this->up(idx);
	return (this->send(sockfd));
}

int	Message::send(int sockfd, std::string msg, int prefix)
{
	this->up(msg, prefix);
	return (this->send(sockfd));
}

void	Message::clear()
{
	this->_msg.clear();
}
