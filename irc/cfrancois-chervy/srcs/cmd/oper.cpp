/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   oper.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/12 17:12:44 by chervy            #+#    #+#             */
/*   Updated: 2021/10/26 12:12:04 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <Command.hpp>
#include <Message.hpp>

void	Command::cmdOper(Command & cmd, Server & server)
{
	Message								msg(cmd, server);
	std::list<std::string>::iterator	itCmd = cmd._command.begin();

	
	if (cmd._command.size() < 3)
	{
		msg.send(cmd._user.getSockfd(), ERR_NEEDMOREPARAMS);
		return ;
	}

	std::string const & usr = *(++itCmd);
	std::string const & psw = *(++itCmd);
	if (!server.isOperPassword(usr, psw))
		msg.send(cmd._user.getSockfd(), ERR_PASSWDMISMATCH);
	else
	{
		cmd._user.setOper(1);
		msg.send(cmd._user.getSockfd(), "381 :You are now an IRC operator", 1);
	}
}