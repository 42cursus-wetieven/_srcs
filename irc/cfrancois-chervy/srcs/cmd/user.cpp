/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   user.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/12 17:13:06 by chervy            #+#    #+#             */
/*   Updated: 2021/10/24 18:33:51 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <Command.hpp>
#include <Message.hpp>

static	int	ft_checkUsername(std::string const & user)
{
	int	ret = 1;

	if (!user.empty())
	{
		for (size_t i = 0; user[i]; i++)
		{
			if (!(ft_isAlpha(user[i]) || ft_isDigit(user[i]) || user[i] == '_') || ft_isWhiteSpace(user[i]))
				ret = 0;
		}
	}
	return (ret);
}

void	Command::cmdUser(Command & cmd, Server & server)
{
	Message								msg(cmd, server);
	std::list<std::string>::iterator	itCmd = cmd._command.begin();
	
	if (cmd._command.size() < 5)
	{
		msg.send(cmd._user.getSockfd(), ERR_NEEDMOREPARAMS);
		return ;
	}

	itCmd++;
	if (!ft_checkUsername(*itCmd))
	{
		std::string tmp;
		tmp += "Your username is invalid. ";
		tmp += "Please make sure that your username contains only alphanumeric characters.";
		msg.send(cmd._user.getSockfd(), tmp, 2);
		return ;
	}

	if (cmd._user.isRegister())
	{
		msg.send(cmd._user.getSockfd(), ERR_ALREADYREGISTRED);
		return ;
	}

	cmd._user.setUsername(*itCmd);
	if (cmd._user.isRegister())
	{
		msg.send(cmd._user.getSockfd(), RPL_WELCOME);
		msg.send(cmd._user.getSockfd(), RPL_HOST);
		msg.send(cmd._user.getSockfd(), RPL_NBRUSER);
		return ;
	}
}