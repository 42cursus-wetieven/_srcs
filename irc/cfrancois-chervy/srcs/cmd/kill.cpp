/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   kill.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/25 19:09:50 by chervy            #+#    #+#             */
/*   Updated: 2021/10/26 12:23:41 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <Command.hpp>
#include <Message.hpp>

static std::list<User>::iterator get_user(std::list<User> &lstUsers, const std::string & nick)
{
	for (std::list<User>::iterator it = lstUsers.begin(); it != lstUsers.end(); it++)
	{
		if (it->getNickname() == nick)
			return it;
	}
	return lstUsers.end();
}

void	Command::cmdKill(Command & cmd, Server & server)
{
	Message	msg(cmd, server);

	if (!cmd._user.isOper())
	{
		msg.send(cmd._user.getSockfd(), ERR_NOPRIVILEGES);
		return;
	}
	
	if (cmd._command.size() < 2)
	{
		msg.send(cmd._user.getSockfd(), ERR_NEEDMOREPARAMS);
		return ;
	}

	std::string					dest = *(++cmd._command.begin());
	std::list<User>::iterator	us_it = get_user(server.getUsers(), dest);

	if (us_it == server.getUsers().end())
	{
		msg.send(cmd._user.getSockfd(), ERR_NOSUCHNICK);
		return ;
	}
	std::string comment;

	if (cmd._command.size() >= 3)
		comment = *(++cmd._command.begin());
	
	std::string					to_send = (*cmd._command.begin() + " :" + comment + "\n");
	
	msg.send(us_it->getSockfd(), to_send, 1);

	us_it->clearBuffer();
	us_it->upBuffer("QUIT\n");

	Command	part(*us_it);
	part.exec(server);
}