/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pass.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/12 17:12:51 by chervy            #+#    #+#             */
/*   Updated: 2021/10/24 17:09:09 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <Command.hpp>
#include <Message.hpp>

void	Command::cmdPass(Command & cmd, Server & server)
{
	Message								msg(cmd, server);
	std::list<std::string>::iterator	itCmd = cmd._command.begin();

	
	if (cmd._user.isRegister())
	{
		msg.send(cmd._user.getSockfd(), ERR_ALREADYREGISTRED);
		return ;
	}
	if (cmd._command.size() < 2)
	{
		msg.send(cmd._user.getSockfd(), ERR_NEEDMOREPARAMS);
		return ;
	}
	if (cmd._user.isAuth())
		return ;
	
	std::string const & psw = *(++itCmd);
	cmd._user.setAuth(server.isPassword(psw));

	if (cmd._user.isRegister())
	{
		msg.send(cmd._user.getSockfd(), RPL_WELCOME);
		msg.send(cmd._user.getSockfd(), RPL_HOST);
		msg.send(cmd._user.getSockfd(), RPL_NBRUSER);
		return ;
	}
}
