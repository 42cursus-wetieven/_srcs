/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   die.cpp                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/22 13:56:50 by chervy            #+#    #+#             */
/*   Updated: 2021/10/24 17:55:50 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <Command.hpp>
#include <Message.hpp>

void	Command::cmdDie(Command & cmd, Server & server)
{
	Message	msg(cmd, server);

	if (cmd.getUser().isOper() == 0)
	{
		msg.send(cmd._user.getSockfd(), ERR_NOPRIVILEGES);
		return ;
	}

	server.shutdown();
}