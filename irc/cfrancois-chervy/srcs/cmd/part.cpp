/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   part.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/12 17:12:48 by chervy            #+#    #+#             */
/*   Updated: 2021/10/25 17:39:36 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <Command.hpp>
#include <Message.hpp>

void	Command::cmdPart(Command & cmd, Server & server)
{
	Message	msg(cmd, server);

	if (cmd._command.size() < 2)
	{
		msg.send(cmd._user.getSockfd(), ERR_NEEDMOREPARAMS);
		return ;
	}
	
	std::list<std::string> arg = cmd.getCommand();
	std::list<std::string> chan_list = ft_splitInList(*(++arg.begin()));

	if (chan_list.size() > 1)
	{
		Command new_cmd(cmd);

		for (std::list<std::string>::iterator it = chan_list.begin(); it != chan_list.end(); it++)
		{
			std::list<std::string> arg;
			arg.push_back(*cmd.getCommand().begin());
			arg.push_back(*it);

			new_cmd.setCommand(arg);
			new_cmd.exec(server);
		}
	}
	else
	{
		std::list<Channel>::iterator chan = server.getChanByName(*chan_list.begin());

		if (chan != server.getChannels().end())
		{
			if (chan->isRegister(cmd._user))
			{
				Channel::users_type const & usr = chan->getUsers();
				std::string tmp = "PART " + chan->getName();
				msg.up("PART " + chan->getName(), 1);
				for (Channel::users_const_iterator us_it = usr.begin(); us_it != usr.end(); us_it++)
				{
					msg.send(us_it->first.getSockfd());
				}
				chan->delUser(cmd._user);
				if (chan->getUsers().size() == 0)
				{
					server.getChannels().erase(chan);
				}
			}
			else
			{
				msg.send(cmd._user.getSockfd(), ERR_NOTONCHANNEL);
			}
		}
		else
		{
			msg.send(cmd._user.getSockfd(), ERR_NOSUCHNICK);
		}
	}
}