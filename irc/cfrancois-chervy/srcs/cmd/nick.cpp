/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   nick.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/12 17:12:41 by chervy            #+#    #+#             */
/*   Updated: 2021/10/25 13:48:48 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <Command.hpp>
#include <Message.hpp>

static	int	ft_checkNickname(std::string const & nick)
{
	if (nick.empty() || nick.size() > 9)
		return (0);
	if (ft_isDigit(nick[0]))
		return (0);
	else if (!nick.empty())
	{
		for (size_t i = 0; nick[i]; i++)
		{
			if (!(ft_isAlpha(nick[i]) || ft_isDigit(nick[i]) || nick[i] == '_') || ft_isWhiteSpace(nick[i]))
				return (0);
		}
	}
	return (1);
}

void	Command::cmdNick(Command & cmd, Server & server)
{
	Message	msg(cmd, server);

	/*
	**	Check params.
	*/
	if (cmd._command.size() < 2)
	{
		msg.send(cmd._user.getSockfd(), ERR_NONICKNAMEGIVEN);
		return ;
	}
	std::string nickname = cmd.getParam(1);

	/*
	**	Check nickname.
	*/
	if (!ft_checkNickname(nickname))
	{
		msg.send(cmd._user.getSockfd(), ERR_ERRONEUSNICKNAME);
		return ;
	}

	for (std::list<User>::iterator it = server.getUsers().begin(); it != server.getUsers().end(); it++)
	{
		if ((*it).getNickname() == nickname)
		{
			msg.send(cmd._user.getSockfd(), ERR_NICKNAMEINUSE);
			return ;
		}
	}

	/*
	** Send messages & set nickname.
	*/
	if (cmd._user.isRegister())
	{	
		std::list<int>					lstSockfd;
		
		const std::list<Channel *> &			lstChan = cmd.getUser().getChans();
		for (std::list<Channel *>::const_iterator itChan = lstChan.begin(); itChan != lstChan.end(); itChan++)
		{
			const std::list< std::pair<User &, int> > &	lstUsers = (*itChan)->getUsers();
			for (std::list< std::pair<User &, int> >::const_iterator itUser = lstUsers.begin(); itUser != lstUsers.end(); itUser++)
			{
				if (itUser->first.getSockfd() != cmd.getUser().getSockfd())
					lstSockfd.push_back(itUser->first.getSockfd());
			}
		}

		lstSockfd.sort();
		lstSockfd.unique();

		msg.up(cmd.getParam(0) + " :" + cmd.getParam(1), 1);
		msg.send(cmd._user.getSockfd());
		for (std::list<int>::iterator it = lstSockfd.begin(); it != lstSockfd.end(); it++)
		{
			msg.send(*it);
		}

		cmd._user.setNickname(nickname);
		return ;
	}
	else
	{
		cmd._user.setNickname(nickname);
		if (cmd._user.isRegister())
		{
			msg.send(cmd._user.getSockfd(), RPL_WELCOME);
			msg.send(cmd._user.getSockfd(), RPL_HOST);
			msg.send(cmd._user.getSockfd(), RPL_NBRUSER);
			return ;
		}
	}
}