/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   notice.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/25 19:10:00 by chervy            #+#    #+#             */
/*   Updated: 2021/10/25 19:23:47 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <Message.hpp>
#include <Command.hpp>

static std::list<User>::const_iterator get_user(std::list<User> const &lstUsers, const std::string & nick)
{
	for (std::list<User>::const_iterator it = lstUsers.begin(); it != lstUsers.end(); it++)
	{
		if (it->getNickname() == nick)
			return it;
	}
	return lstUsers.end();
}

void	Command::cmdNotice(Command & cmd, Server & server)
{
	Message	msg(cmd, server);

	if (cmd._command.size() == 1)
	{
		return ;
	}
	if (cmd._command.size() == 2)
	{
		return ;
	}

	std::list<std::string>		arg = cmd.getCommand();
	std::list<std::string>		recvs = ft_splitInList(*(++arg.begin()));
	std::string					to_send = *(++(++(arg.begin())));

	if (recvs.size() > 1)
	{
		Command new_cmd(cmd);

		for (std::list<std::string>::iterator it = recvs.begin(); it != recvs.end(); it++)
		{
			std::list<std::string> arg;
			arg.push_back(*cmd.getCommand().begin());
			arg.push_back(*it);
			arg.push_back(to_send);

			new_cmd.setCommand(arg);
			new_cmd.exec(server);
		}
	}
	else
	{
		std::string & dest = *recvs.begin();
		if (server.isChan(dest))
		{
			std::list<Channel>::iterator chan = server.getChanByName(dest);
			if (chan == server.getChannels().end())
			{
				return ;
			}
			else if (!chan->isRegister(cmd._user))
			{
				return ;
			}
			else
			{
				Channel::users_type lst = chan->getUsers();
				msg.up("NOTICE " + chan->getName() +  " :" + to_send, 1);
				for (Channel::users_const_iterator us_it = lst.begin(); us_it != lst.end(); us_it++)
				{
					if (cmd._user.getNickname() != us_it->first.getNickname())
					{
						msg.send(us_it->first.getSockfd());
					}
				}
			}
		}
		else
		{
			std::list<User>::const_iterator us_it = get_user(server.getUsers(), dest);
			if (us_it == server.getUsers().end())
			{
				return ;
			}
			else
			{	
				msg.send(us_it->getSockfd(), "NOTICE " + dest +  " :" + to_send, 1);
			}
		}
	}
}
