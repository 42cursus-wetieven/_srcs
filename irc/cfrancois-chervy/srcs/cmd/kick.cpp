/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   kick.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/12 17:12:36 by chervy            #+#    #+#             */
/*   Updated: 2021/10/25 17:38:59 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <Command.hpp>
#include <Message.hpp>
#include <Channel.hpp>

void	Command::cmdKick(Command & cmd,  Server & server)
{
	Message		msg(cmd, server);
	std::string	tmp;
	
	/*
	**	Check Params.
	*/
	if (cmd._command.size() < 3)
	{
		msg.send(cmd._user.getSockfd(), ERR_NEEDMOREPARAMS);
		return;
	}

	tmp = cmd.getParam(1);
	std::list<std::string>	argChannels = ft_splitInList(tmp);
	tmp = cmd.getParam(2);
	std::list<std::string>	argUsers = ft_splitInList(tmp);

	if (argChannels.size() != argUsers.size())
	{
		return;
	}

	std::list<std::string>::iterator	itArgChannels = argChannels.begin();
	std::list<std::string>::iterator	itArgUsers = argUsers.begin();

	while (itArgChannels != argChannels.end() && itArgUsers != argUsers.end())
	{
		/*
		**	Check Channel.
		*/
		std::list<Channel>::iterator	itLstChannel;
		std::list<Channel>				&lstChannels = server.getChannels();
		itLstChannel = server.getChanByName(*itArgChannels);
		if (itLstChannel == lstChannels.end())
		{
			msg.send(cmd._user.getSockfd(), ERR_NOSUCHCHANNEL);
			return;
		}
		Channel	&channel = *itLstChannel;
	
		/*
		**	Check Privilege.
		*/
		const Channel::users_type		&lstUser = channel.getUsers();
		Channel::users_const_iterator	itLstUser;
		itLstUser = channel.getUserByNickname(cmd._user.getNickname());
		if (itLstUser == lstUser.end())
		{
			msg.send(cmd._user.getSockfd(), ERR_NOTONCHANNEL);
			return;
		}
		else if (!itLstUser->second)
		{
			msg.send(cmd._user.getSockfd(), ERR_CHANOPRIVSNEEDED);
			return;
		}
		
		/*
		**	Check User.
		*/
		itLstUser = channel.getUserByNickname(*itArgUsers);
		if (itLstUser == lstUser.end())
			return;
		User	&user = itLstUser->first;
	
		/*
		**	Kick User.
		*/
		tmp.clear();
		if (cmd._command.size() >= 3)
		{
			tmp = "KICK " + channel.getName() + " " + user.getNickname() + " " + cmd.getParam(3);
		}
		else
		{
			tmp = "KICK " + channel.getName() + " " + user.getNickname();
		}
		itLstUser = lstUser.begin();
		while (itLstUser != lstUser.end())
		{
			msg.send(itLstUser->first.getSockfd(), tmp, 1);
			itLstUser++;
		}
		channel.delUser(user);

		/*
		** Increment
		*/		
		itArgChannels++;
		itArgUsers++;
	}
}