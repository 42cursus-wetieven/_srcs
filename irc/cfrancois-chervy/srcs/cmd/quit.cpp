/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   quit.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/12 17:13:01 by chervy            #+#    #+#             */
/*   Updated: 2021/10/26 09:49:56 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <Command.hpp>
#include <Message.hpp>

void	Command::cmdQuit(Command & cmd, Server & server)
{
	Message	msg(cmd, server);

	/*
	** Fix Arg
	*/
	if (cmd._command.size() >= 2)
		msg.up(cmd.getParam(0) + " :" + cmd.getParam(1), 1);
	else
		msg.up(cmd.getParam(0) + " :Client Quit", 1);

	/*
	** Get all fds to send quit
	*/
	std::list<Channel *> const	&user_chans = cmd.getUser().getChans();
	std::list<int> 				fds;

	for (std::list<Channel *>::const_iterator it = user_chans.begin(); it != user_chans.end(); it++)
	{
		Channel const & chan = *(*it);
		Channel::users_type const & users = chan.getUsers();

		
		for (Channel::users_const_iterator us = users.begin(); us != users.end(); us++)
		{
			if (cmd.getUser().getSockfd() != us->first.getSockfd())
				fds.push_back(us->first.getSockfd());
		}
	}
	fds.sort();
	fds.unique();

	/*
	** Send Quit
	*/
	for (std::list<int>::iterator it = fds.begin(); it != fds.end(); it++)
	{
		msg.send(*it);
	}

	/*
	** delUser from chans
	*/
	std::list<Channel> &chans = server.getChannels();
	for (std::list<Channel>::iterator it = chans.begin(); it != chans.end();)
	{
		it->delUser(cmd._user);
		if (it->getUsers().size() == 0)
			chans.erase(it++);
		else
			it++;
	}

	cmd._user.deconnect();
}