/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   join.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/12 17:04:19 by chervy            #+#    #+#             */
/*   Updated: 2021/10/26 10:01:46 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <Command.hpp>
#include <Message.hpp>

void	Command::cmdJoin(Command & cmd, Server & server)
{
	Message	msg(cmd, server);

	if (cmd._command.size() < 2)
	{
		msg.send(cmd._user.getSockfd(), ERR_NEEDMOREPARAMS);
		return ;
	}

	std::list<std::string> arg = cmd.getCommand();
	std::list<std::string> chan_list = ft_splitInList(*(++arg.begin()));
	std::list<std::string> key_list;
	if (cmd._command.size() == 3)
		key_list = ft_splitInList(*(++(++(arg.begin()))));
	while (key_list.size() < chan_list.size())
		key_list.push_back("");

	if (chan_list.size() > 1)
	{
		Command new_cmd(cmd);

		std::list<std::string>::iterator key_it = key_list.begin();
		for (std::list<std::string>::iterator it = chan_list.begin(); it != chan_list.end(); it++, key_it++)
		{
			std::list<std::string> arg;
			arg.push_back(*cmd.getCommand().begin());
			arg.push_back(*it);
			arg.push_back(*key_it);

			new_cmd.setCommand(arg);
			new_cmd.exec(server);
		}
	}
	else
	{
		if (cmd._user.getChans().size() >= server._maxChanByUser)
		{
			msg.send(cmd._user.getSockfd(), ERR_TOOMANYCHANNELS);
			return ;
		}
		if (!server.isChan(*chan_list.begin()))
		{
			msg.send(cmd._user.getSockfd(), ERR_NOSUCHCHANNEL);
			return ;
		}
		std::list<Channel>::iterator chan = server.getChanByName(*chan_list.begin());
		if (chan != server.getChannels().end())
		{
			if (chan->isRegister(cmd._user))
			{
				return;
			}
			else
			{
				if (!chan->addUser(cmd._user, *key_list.begin()))
				{
					msg.send(cmd._user.getSockfd(), ERR_BADCHANNELKEY);
					return ;
				}
			}
		}
		else
		{
			Channel to_add(*chan_list.begin(), *key_list.begin());
			
			server.getChannels().push_back(to_add);
			server.getChannels().back().addUser(cmd._user, *key_list.begin());
		}

		chan = server.getChanByName(*chan_list.begin());
		if (chan != server.getChannels().end())
		{
			Channel::users_type usr = chan->getUsers();

			std::string tmp = "JOIN " + chan->getName();
			msg.up("JOIN " + chan->getName(), 1);
			for (Channel::users_const_iterator us_it = usr.begin();  us_it != usr.end(); us_it++)
			{
				msg.send(us_it->first.getSockfd());
			}
			
			msg.send(cmd._user.getSockfd(), RPL_NAMEREPLY);
			msg.send(cmd._user.getSockfd(), RPL_ENDOFNAMES);
		}
	}
}