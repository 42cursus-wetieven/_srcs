/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ping.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/24 14:13:29 by chervy            #+#    #+#             */
/*   Updated: 2021/10/24 17:28:13 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <Command.hpp>
#include <Message.hpp>

void	Command::cmdPing(Command & cmd, Server & server)
{
	Message	msg(cmd, server);
	std::string	tmp;

	if (cmd._command.size() < 2)
	{
		msg.send(cmd._user.getSockfd(), ERR_NEEDMOREPARAMS);
		return ;
	}

	msg.up(":" + server.getHost() + " " + "PONG " + server.getHost() + " :" + cmd.getParam(1));
	msg.send(cmd._user.getSockfd());
}
