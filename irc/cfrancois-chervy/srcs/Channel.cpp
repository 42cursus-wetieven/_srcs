/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Channel.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/08 16:25:00 by chervy            #+#    #+#             */
/*   Updated: 2021/10/26 12:26:12 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <Channel.hpp>

Channel::Channel() {}

Channel::Channel(Channel const & channel)
{
	this->_name = channel._name;
	this->_key = channel._key;
	this->_users = channel._users;
}

Channel::Channel(std::string const & name, std::string const & key)
{
	this->_name = name;
	this->_key = key;
}

Channel::~Channel() {}

Channel &	Channel::operator=(Channel const & rhs)
{
	this->_name = rhs._name;
	this->_key = rhs._key;
	this->_users = rhs._users;
	return (*this);
}

std::string	Channel::getName() const
{
	return (this->_name);
}

Channel::users_type	const & Channel::getUsers(void) const
{
	return (this->_users);
}

Channel::users_const_iterator Channel::getUserByNickname(const std::string & nick) const
{
	Channel::users_const_iterator it = this->_users.begin();
	Channel::users_const_iterator end = this->_users.end();

	while (it != end)
	{
		if (it->first.getNickname() == nick)
			return (it);
		it++;
	}
	return (it);
}

void	Channel::setGrade(User const & user, int const & grade)
{
	users_iterator	it;
	
	for(it = this->_users.begin(); it != this->_users.end(); it++)
	{
		std::pair<User &, int>	&tmp = *it;
		if (const_cast<User &>(user) == tmp.first)
		{
			tmp.second = grade;
		}
	}
}

int	Channel::isRegister(User const & user)
{
	users_iterator	it;

	for(it = this->_users.begin(); it != this->_users.end(); it++)
	{
		std::pair<User &, int>	&tmp = *it;
		if (const_cast<User &>(user) == tmp.first)
		{
			return (1);
		}
	}
	return (0);
}

int	Channel::isOper(User const & user)
{
	users_const_iterator	it = _users.begin();
	users_const_iterator	end = _users.end();

	while (it != end)
	{
		if ((*it).first == user && (*it).second)
		{
			return (1);
		}
		it++;
	}
	return (0);
}

/*
** Need to check key ???
*/
int		Channel::addUser(User & user, std::string const & key)
{
	if (!isRegister(user) && (key == this->_key || this->_key.empty()))
	{
		if (this->_users.size() != 0)
		{
			std::pair<User &, int>	tmp(const_cast<User &>(user), 0);
			this->_users.push_back(tmp);
			user.addChan(this);
			return 1;
		}
		else
		{
			std::pair<User &, int>	tmp(const_cast<User &>(user), 1);
			this->_users.push_back(tmp);
			user.addChan(this);
			return 1;
		}
	}
	return 0;
}

void	Channel::delUser(User & user)
{
	users_iterator	it;

	for(it = this->_users.begin(); it != this->_users.end(); it++)
	{
		std::pair<User &, int>	&tmp = *it;
		if (const_cast<User &>(user) == tmp.first)
		{
			this->_users.erase(it);
			user.delChan(this);
			return;
		}
	}
}
