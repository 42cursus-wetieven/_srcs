/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/13 14:44:53 by chervy            #+#    #+#             */
/*   Updated: 2021/10/25 17:41:10 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

# include <ircserv.hpp>

int	ft_isAlpha(char c)
{
	return ((c >= 'A' && c <= 'Z') || ((c >= 'a' && c <= 'z')));
}

int	ft_isDigit(char c)
{
	return (c >= '0' && c <= '9');
}

int	ft_isWhiteSpace(char c)
{
	return ((c >= 9 && c <= 13) || c == 32);
}

int	ft_atoi(std::string str)
{
	std::stringstream		ss;
	int						i;

	ss << str;
	ss >> i;
	return (i);
}

std::string	ft_stoa(size_t i)
{
	std::stringstream		ss;
	std::string				str;

	ss << i;
	ss >> str;
	return (str);
}

std::string	ft_toUpper(std::string str)
{
	for (size_t i = 0; str[i]; i++)
	{
		str[i] = std::toupper(str[i]);
	}
	return (str);
}

std::list<std::string> ft_splitInList(std::string & to_split)
{
	std::list<std::string> ret;
	std::string::iterator idx = to_split.begin();
	std::string::iterator it;
	for (it = idx; it != to_split.end(); it++)
	{
		if (*it == ',')
		{
			ret.push_back(to_split.substr(idx - to_split.begin(), it - idx));
			idx = it + 1;
		}
	}
	ret.push_back(to_split.substr(idx - to_split.begin(), it - idx));
	return ret;
}
