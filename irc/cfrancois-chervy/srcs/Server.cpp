/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Server.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/25 15:48:07 by chervy            #+#    #+#             */
/*   Updated: 2021/10/26 12:12:23 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <ircserv.hpp>
#include <Server.hpp>
#include <Command.hpp>
#include <cstring>

/*
** Constructor & Destructor
*/

Server::Server() :
	_port(6667),
	_server_status(1),
	_oper_password("pass"),
	_oper_user("admin") {}

Server::Server(const int port, const std::string & psw) :
	_port(port),
	_server_status(1),
	_password(psw),
	_oper_password("pass"),
	_oper_user("admin") {}

Server::~Server() {}

std::list<User> &		Server::getUsers()
{
	return (this->_users);
}

const std::list<User> &		Server::getUsers(void) const
{
	return (this->_users);
}

std::list<Channel> &	Server::getChannels()
{
	return (this->_chans);
}

std::list<Channel> const &	Server::getChannels() const
{
	return (this->_chans);
}

const std::string &	Server::getHost() const
{
	return (this->_host);
}

std::list<Channel>::iterator	Server::getChanByName(const std::string &name)
{
	for (std::list<Channel>::iterator it = this->_chans.begin(); it != this->_chans.end(); it++)
	{
		if (it->getName() == name)
			return it;
	}
	return this->_chans.end();
}

std::list<Channel>::const_iterator	Server::getChanByName(const std::string &name) const
{
	for (std::list<Channel>::const_iterator it = this->_chans.begin(); it != this->_chans.end(); it++)
	{
		if (it->getName() == name)
			return it;
	}
	return this->_chans.end();
}

int	Server::isChan(std::string ref) const
{
	if  (ref.size() > this->_maxLenChanName)
		return 0;
	if (ref.size() < 2)
		return 0;
	if (!(ref.front() == '#' || ref.front() == '&'))
		return 0;
	for (std::string::const_iterator it = ++(ref.begin()); it != ref.end(); it++)
	{
		if(*it == ' ' || *it == 7 || *it == ',')
			return 0;
	}
	return 1;
}

int Server::isPassword(std::string const & psw) const
{
	if (_password.empty())
		return 1;
	if (_password == psw)
		return 1;
	return 0;
}

int Server::isOperPassword(std::string const & user, std::string const & psw) const
{
	if (_oper_password.empty())
		return 0;
	if (_oper_password == psw && user == _oper_user)
		return 1;
	return 0;
}

int Server::run() 
{
	int							ret;
	int							current_size;
	std::list<User>::iterator	users_it;

	ret = init();
	if (ret == 1)
		return (ret);

	while (_server_status == 1)
	{
		ret = poll(&_fds[0], _fds.size(), _timeout);
		if (ret == 0)
			_server_status = 0;
		if (ret < 0)
		{
			std::perror("poll()");
			break ;
		}

		if (_server_status == 1)
		{	
			current_size	= _fds.size();
			users_it		= _users.begin();

			if (_fds[0].revents & POLLIN)
			{
				ret = new_connection();
				if (ret == -1)
					break ;
			}

			for (int i = 1; i < current_size && ret >= 0; i++)
			{
				if (_fds[i].revents == 0)
					++users_it;
				else if (_fds[i].revents & POLLHUP)
					ret = disconnect(_fds.begin() + i, *users_it);
				else if (_fds[i].revents & POLLIN)
					ret = receive(_fds[i].fd, *users_it);
				else
					++users_it;
			}

			if (_server_status == 1)
			{
				ret = launchCommand();
				if (ret == -1)
					break ;
			}
		}
	}
	if (server_close() == -1)
		ret = -1;

	return (ret < 0);
}

void	Server::shutdown()
{
	this->_server_status = 0;
}

/*
** Server Context
*/

/*
** Init socket to start poll context
*/
int Server::init()
{
	char host[512];
	if (gethostname(host, 512) == -1)
	{
		std::perror("gethostname()");
		return 1;
	}
	this->_host = host;

	_socket = socket(PF_INET, SOCK_STREAM, 0);
	if (_socket == -1)
	{
		std::perror("socket()");
		return 1;
	}

	if (fcntl(_socket, F_SETFL,  O_NONBLOCK) < 0)
	{
		std::perror("fcntl()");
		close(_socket);
		return 1;
	}

	_sin.sin_addr.s_addr = htonl(INADDR_ANY);
	_sin.sin_family = PF_INET;
	_sin.sin_port = htons(_port);

	int enable = 1;
	if (setsockopt(_socket, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(enable)) < 0)
	{
		std::perror("setsockopt()");
		close(_socket);
		return 1;
	}

	if (bind(_socket, (sockaddr *)(&_sin), sizeof(_sin)))
	{
		std::perror("bind()");
		close(_socket);
		return 1;
	}

	if (listen(_socket, 64) == -1)
	{
		std::perror("listen()");
		close(_socket);
		return 1;
	}

	_fds.push_back(pollfd());
	_fds.front().fd = _socket;
	_fds.front().events = POLLIN;

	return 0;
}

/*
**	accept new connection & init new user
*/
int Server::new_connection()
{
	int					new_fd;
	sockaddr_in			sin;
	socklen_t			len = sizeof(sin);
	
	
	new_fd = accept(_socket, (sockaddr *)&sin, &len);
	if (new_fd == -1)
	{
		shutdown();
		std::perror("accept()");
		return -1;
	}
	if (_maxConnection <= this->_users.size())
	{
		if (close(new_fd) == -1)
		{
			shutdown();
			std::perror("close()");
			return -1;
		}
		return 0;
	}
	_fds.push_back(pollfd());
	_fds.back().fd = new_fd;
	_fds.back().events = POLLIN;

	User				usr;
	usr.setHost(inet_ntoa(sin.sin_addr));
	usr.setSockfd(new_fd);
	usr.setAuth(isPassword(""));
	
	_users.push_back(usr);
	logConnect(new_fd, _users.back().getHost());
	return 0;
}

/*
** On disconnect change cmd buffer with quit
*/
int Server::disconnect(std::vector<pollfd>::iterator const &it, User & usr)
{
	logDisconnect((*it).fd);
	usr.clearBuffer();
	usr.upBuffer("QUIT :client disconnect\n");
	return 0;
}

/*
** Read buffer of user
*/
int Server::receive(int fd, User & usr)
{
	char buffer[513];
	int ret;

	std::memset(buffer, 0, sizeof(buffer));
	ret = recv(fd, buffer, sizeof(buffer) - 1, 0);
	if (ret >= 0)
	{
		usr.upBuffer(buffer);
	}
	if (ret < 0)
	{
		shutdown();
		std::perror("recv()");
		return -1;
	}
	return 0;
}

/*
** launch ready cmd & rm disconnected users
*/
int	Server::launchCommand(void)
{
	int	ret = 0;

	for (std::list<User>::iterator it = _users.begin(); it != _users.end(); it++)
	{
		while (it->isReadyBuffer())
		{
			Command cmd(*it);
			cmd.exec(*this);
		}
	}

	std::list<User>::iterator usr = _users.begin();
	
	while (usr != _users.end())
	{
		if (!usr->isConnected())
		{
			for (std::vector<pollfd>::iterator fd = _fds.begin(); fd != _fds.end(); fd++)
			{
				if (usr->getSockfd() == fd->fd)
				{
					ret = close(fd->fd);
					_fds.erase(fd);
					_users.erase(usr++);
					if (ret == -1)
					{
						shutdown();
						std::perror("close()");
						return -1;
					}
					break ;
				}
			}
		}
		else
		{
			usr++;
		}
	}
	return 0;
}

/*
** Close all connection and clear chans & users
*/
int	Server::server_close()
{
	int		ret;

	ret = 0;
	for (std::vector<pollfd>::iterator it = _fds.begin(); it != _fds.end(); it++)
	{
		if (it->fd != -1)
		{
			if (close(it->fd) == -1)
			{
				std::perror("close()");
				ret = -1;
			}
		}
	}
	_fds.clear();
	_chans.clear();
	_users.clear();
	
	return ret;
}

/*
** Server utility ***
*/

void	Server::logConnect(int const & fd, std::string const & host)
{
	std::cout << "---------------------------------------------------------" << std::endl;
	std::cout << "New client connected." << std::endl;
	std::cout << "FD	: " << fd << std::endl;
	std::cout << "From	: " << host << std::endl;
	std::cout << "---------------------------------------------------------" << std::endl;
}

void	Server::logDisconnect(int const & fd)
{
	std::cout << "---------------------------------------------------------" << std::endl;
	std::cout << "Client disconnected." << std::endl;
	std::cout << "FD	: " << fd << std::endl;
	std::cout << "---------------------------------------------------------" << std::endl;
}
