#include <Message.hpp>

std::string	Message::msgNotOnChannel(Command const & cmd, Server const &server)
{
	std::string	msg;
	std::list<std::string>::const_iterator	itCmd = cmd.getCommand().begin();

	itCmd++;
	msg += ":" + server.getHost() + " " + ERR_NOTONCHANNEL_STR + " ";
	msg += cmd.getUser().getNickname() + " ";
	msg += *itCmd + " :You're not on that channel\n";

	return (msg);
}
