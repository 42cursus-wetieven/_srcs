/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   251_NbrUsers.cpp                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/15 14:32:58 by chervy            #+#    #+#             */
/*   Updated: 2021/10/25 15:52:14 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <Message.hpp>

std::string	Message::msgNbrUsers(Command const & cmd, Server const & server)
{
	std::string	msg;

	msg += ":" +  server.getHost() + " 251 "+ cmd.getUser().getNickname() + " ";
	msg += ":There are " + ft_stoa(server.getUsers().size()) + " users on server\n";
	return (msg);
}
