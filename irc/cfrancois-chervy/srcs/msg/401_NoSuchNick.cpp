/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   401_NoSuchNick.cpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/25 19:06:35 by chervy            #+#    #+#             */
/*   Updated: 2021/10/25 19:08:12 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <Message.hpp>

std::string	Message::msgNoSuchNick(Command const & cmd, Server const &server)
{
	std::string	msg;
	std::list<std::string>::const_iterator	itCmd = (cmd.getCommand().begin());

	itCmd++;
	msg += ":" + server.getHost() + " " + ERR_NOSUCHNICK_STR + " ";
	msg += cmd.getUser().getNickname() + " ";
	msg += *itCmd + " :No such nick/channel\n";
	
	return (msg);
}