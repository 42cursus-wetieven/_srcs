/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   431_NoNicknameGiven.cpp                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/15 15:15:34 by chervy            #+#    #+#             */
/*   Updated: 2021/10/25 19:14:03 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <Message.hpp>

std::string	Message::msgNoNicknameGiven(Command const & cmd, Server const &server)
{
	std::string	msg;

	msg += ":" + server.getHost() + " " + ERR_NONICKNAMEGIVEN_STR + " ";
	msg += cmd.getUser().getNickname() + " ";
	msg += ":No nickname given\n";
	return (msg);
}
