/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   436_NickCollision.cpp                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/15 15:16:00 by chervy            #+#    #+#             */
/*   Updated: 2021/10/25 15:23:29 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <Message.hpp>

std::string	Message::msgNickCollision(Command const & cmd, Server const &server)
{
	std::string	msg;
	std::list<std::string>::const_iterator	itCmd = cmd.getCommand().begin();

	itCmd++;
	msg += ":" + server.getHost() + " " + ERR_NICKCOLLISION_STR + " ";
	msg += cmd.getUser().getNickname() + " ";
	msg += *itCmd + ":Nickname collision KILL\n";
	return (msg);
}
