/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   002_Host.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/15 14:32:55 by chervy            #+#    #+#             */
/*   Updated: 2021/10/25 15:20:47 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <Message.hpp>

std::string	Message::msgHost(Command const & cmd, Server const &server)
{
	std::string	msg;

	msg += ":" + server.getHost() + " 002 "+ cmd.getUser().getNickname() + " ";
	msg += ":Your host is " + server.getHost() + ", running version 0.42\n";
	return (msg);
}
