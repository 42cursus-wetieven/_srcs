/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   353_NameReply.cpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/25 19:10:43 by chervy            #+#    #+#             */
/*   Updated: 2021/10/25 19:10:45 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <Message.hpp>

static std::string getUsersList(Channel const & channel)
{
	Channel::users_type	users = channel.getUsers();
	std::string list;

	for (Channel::users_const_iterator it = users.begin(); it != users.end(); it++)
	{
		if (list.size() != 0)
			list += " ";
		if (it->second == 1)
			list += '@';
		list += it->first.getNickname();
	}
	return list;
}

std::string	Message::msgNameReply(Command const & cmd, Server const & server)
{
	std::string	msg;
	std::list<std::string> const & args = cmd.getCommand();

	if (args.size() >= 2)
	{
		std::string const & chan_name = *(++args.begin());
		
		std::list<Channel>::const_iterator	chan = server.getChanByName(chan_name);
		Channel::users_type const			&users = chan->getUsers();
		char								c = '=';
		for (Channel::users_const_iterator it = users.begin(); it != users.end(); it++)
		{
			if (it->second == 1)
			{
				c = '@';
				break ;
			}
		}
		msg += ":" + server.getHost() + " " ;
		msg += "353 ";
		msg += cmd.getUser().getNickname() + " " + c + " " + chan->getName() + " :" + getUsersList(*chan) + "\n";
	}
	return (msg);
}
