/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   412_NoTextToSend.cpp                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/25 19:11:07 by chervy            #+#    #+#             */
/*   Updated: 2021/10/25 19:14:01 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <Message.hpp>

std::string	Message::msgNoTextToSend(Command const & cmd, Server const &server)
{
	std::string	msg;

	msg += ":" + server.getHost() + " " + ERR_NOTEXTTOSEND_STR + " ";
	msg += cmd.getUser().getNickname() + " ";
	msg += " :No text to send\n";

	return (msg);
}
