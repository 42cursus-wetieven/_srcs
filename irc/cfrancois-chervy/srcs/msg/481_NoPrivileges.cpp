/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   481_NoPrivileges.cpp                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/22 13:49:25 by chervy            #+#    #+#             */
/*   Updated: 2021/10/25 15:24:13 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <Message.hpp>

std::string	Message::msgNoPrivileges(Command const & cmd, Server const &server)
{
	std::string	msg;

	msg += ":" + server.getHost() + " " + ERR_NOPRIVILEGES_STR + " ";
	msg += cmd.getUser().getNickname() + " ";
	msg += ":Permission Denied- You're not an IRC operator\n";
	return (msg);
}
