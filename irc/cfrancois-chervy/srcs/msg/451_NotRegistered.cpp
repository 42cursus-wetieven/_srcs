/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   451_NotRegistered.cpp                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/15 16:19:11 by chervy            #+#    #+#             */
/*   Updated: 2021/10/25 15:23:44 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <Message.hpp>

std::string	Message::msgNotRegistered(Command const & cmd, Server const &server)
{
	std::string	msg;
	std::list<std::string>::const_iterator	itCmd = cmd.getCommand().begin();

	itCmd++;
	msg += ":" + server.getHost() + " " + ERR_NOTREGISTERED_STR + " ";
	msg += cmd.getUser().getNickname() + " ";
	msg += ":You have not registered\n";
	return (msg);
}
