/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   475_BadChannelKey.cpp                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/26 09:53:11 by chervy            #+#    #+#             */
/*   Updated: 2021/10/26 09:55:04 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <Message.hpp>

std::string	Message::msgBadChannelKey(Command const & cmd, Server const &server)
{
	std::string	msg;

	msg = ":" + server.getHost() + " " + ERR_BADCHANNELKEY_STR + " " ;
	msg += cmd.getUser().getNickname() + " " + cmd.getParam(1);
	msg += cmd.getParam(2) + " :Cannot join channel (+k)\n";
	return (msg);
}
