/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   404_CannotSendToChan.cpp                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/25 19:08:18 by chervy            #+#    #+#             */
/*   Updated: 2021/10/25 19:08:19 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <Message.hpp>

std::string	Message::msgCannotSendToChan(Command const & cmd, Server const &server)
{
	std::string	msg;
	std::list<std::string>::const_iterator	itCmd = cmd.getCommand().begin();

	itCmd++;
	msg += ":" + server.getHost() + " " + ERR_CANNOTSENDTOCHAN_STR + " ";
	msg += cmd.getUser().getNickname() + " ";
	msg += *itCmd + " :Cannot send to channel\n";

	return (msg);
}