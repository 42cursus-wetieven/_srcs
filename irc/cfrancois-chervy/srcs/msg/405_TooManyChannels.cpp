/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   405_TooManyChannels.cpp                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/25 19:10:55 by chervy            #+#    #+#             */
/*   Updated: 2021/10/25 19:31:16 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <Message.hpp>

std::string	Message::msgTooManyChannels(Command const & cmd, Server const &server)
{
	std::string	msg;
	std::list<std::string>::const_iterator	itCmd = cmd.getCommand().begin();

	itCmd++;
	msg += ":" + server.getHost() + " " + ERR_TOOMANYCHANNELS_STR + " ";
	msg += cmd.getUser().getNickname() + " ";
	msg += *itCmd + " :You have joined too many channels\n";

	return (msg);
}
