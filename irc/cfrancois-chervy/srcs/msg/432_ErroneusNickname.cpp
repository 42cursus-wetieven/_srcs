/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   432_ErroneusNickname.cpp                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/15 15:15:54 by chervy            #+#    #+#             */
/*   Updated: 2021/10/25 15:23:08 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <Message.hpp>

std::string	Message::msgErroneusNickname(Command const & cmd, Server const &server)
{
	std::string	msg;
	std::list<std::string>::const_iterator	itCmd = cmd.getCommand().begin();

	itCmd++;
	msg += ":" + server.getHost() + " " + ERR_ERRONEUSNICKNAME_STR + " ";
	msg += cmd.getUser().getNickname() + " ";
	msg += *itCmd + " :Erroneus nickname\n";
	return (msg);
}
