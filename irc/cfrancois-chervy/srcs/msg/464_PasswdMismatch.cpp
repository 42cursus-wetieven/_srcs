/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   464_PasswdMismatch.cpp                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/26 12:08:39 by chervy            #+#    #+#             */
/*   Updated: 2021/10/26 12:09:13 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <Message.hpp>

std::string	Message::msgPasswdMismatch(Command const & cmd, Server const &server)
{
	std::string	msg;

	msg += ":" + server.getHost() + " " + ERR_PASSWDMISMATCH_STR + " ";
	msg += cmd.getUser().getNickname() + " ";
	msg += ":Password incorrect\n";
	return (msg);
}
