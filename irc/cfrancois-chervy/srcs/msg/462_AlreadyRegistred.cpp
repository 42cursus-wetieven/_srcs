/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   462_AlreadyRegistred.cpp                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/15 15:33:06 by chervy            #+#    #+#             */
/*   Updated: 2021/10/25 15:23:59 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <Message.hpp>

std::string	Message::msgAlreadyRegistred(Command const & cmd, Server const &server)
{
	std::string	msg;

	msg += ":" + server.getHost() + " " + ERR_ALREADYREGISTRED_STR + " ";
	msg += cmd.getUser().getNickname() + " ";
	msg += ":You may not reregister\n";
	return (msg);
}
