/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   403_NoSuchChannel.cpp                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/23 16:11:33 by chervy            #+#    #+#             */
/*   Updated: 2021/10/25 15:21:50 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <Message.hpp>

std::string	Message::msgNoSuchChannel(Command const & cmd, Server const &server)
{
	std::string	msg;
	std::list<std::string>::const_iterator	itCmd = cmd.getCommand().begin();

	itCmd++;
	msg += ":" + server.getHost() + " " + ERR_NOSUCHCHANNEL_STR + " ";
	msg += cmd.getUser().getNickname() + " ";
	msg += *itCmd + " :No such channel\n";

	return (msg);
}