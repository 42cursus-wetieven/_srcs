/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   001_Welcome.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/14 19:05:20 by chervy            #+#    #+#             */
/*   Updated: 2021/10/25 15:19:00 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <Message.hpp>

std::string	Message::msgWelcome(Command const & cmd, Server const &server)
{
	std::string	msg;

	msg += ":" + server.getHost() + " 001 " + cmd.getUser().getNickname() + " ";
	msg += ":Welcome to the ft_irc Internet Relay Network ";
	msg += cmd.getUser().getNickname() + "!" + cmd.getUser().getUsername() + "@" + cmd.getUser().getHost();
	msg += "\n";
	return (msg);
}
