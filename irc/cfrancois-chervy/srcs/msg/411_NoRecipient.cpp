/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   411_NoRecipient.cpp                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/25 19:11:03 by chervy            #+#    #+#             */
/*   Updated: 2021/10/25 19:11:04 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <Message.hpp>

std::string	Message::msgNoRecipient(Command const & cmd, Server const &server)
{
	std::string	msg;

	msg += ":" + server.getHost() + " " + ERR_NORECIPIENT_STR + " ";
	msg += cmd.getUser().getNickname() + " ";
	msg += cmd.getParam(0) + " :No recipient given (<" + cmd.getParam(0) + ">)\n";

	return (msg);
}