/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   476_BadChanMask.cpp                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/22 18:35:56 by chervy            #+#    #+#             */
/*   Updated: 2021/10/25 15:24:05 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <Message.hpp>

std::string	Message::msgBadChanMask(Command const & cmd, Server const &server)
{
	std::string	msg;

	msg += ":" + server.getHost() + " " + ERR_BADCHANMASK_STR + " ";
	msg += cmd.getUser().getNickname() + " ";
	msg += ":You may not reregister\n";
	return (msg);
}
