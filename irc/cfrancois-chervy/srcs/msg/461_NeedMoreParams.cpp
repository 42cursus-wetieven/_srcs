/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   461_NeedMoreParams.cpp                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/15 15:32:47 by chervy            #+#    #+#             */
/*   Updated: 2021/10/25 15:23:53 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <Message.hpp>

std::string	Message::msgNeedMoreParams(Command const & cmd, Server const &server)
{
	std::string	msg;
	std::list<std::string>::const_iterator	itCmd = cmd.getCommand().begin();

	msg += ":" + server.getHost() + " " + ERR_NEEDMOREPARAMS_STR + " ";
	msg += cmd.getUser().getNickname() + " ";
	msg += *itCmd + " :Not enough parameters\n";
	return (msg);
}
