/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   433_NicknameInUse.cpp                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/15 15:15:57 by chervy            #+#    #+#             */
/*   Updated: 2021/10/25 15:23:18 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <Message.hpp>

std::string	Message::msgNicknameInUse(Command const & cmd, Server const &server)
{
	std::string	msg;
	std::list<std::string>::const_iterator	itCmd = cmd.getCommand().begin();

	itCmd++;
	msg += ":" + server.getHost() + " " + ERR_NICKNAMEINUSE_STR + " ";
	msg += cmd.getUser().getNickname() + " ";
	msg += *itCmd + ":Nickname is already in use\n";
	return (msg);
}
