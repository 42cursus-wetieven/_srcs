/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   366_EndOfNames.cpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/24 15:12:33 by chervy            #+#    #+#             */
/*   Updated: 2021/10/25 19:17:53 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <Message.hpp>

std::string	Message::msgEndOfNames(Command const & cmd, Server const &server)
{
	std::string	msg;

	msg = ":" + server.getHost() + " " + RPL_ENDOFNAMES_STR + " " ;
	msg += cmd.getUser().getNickname() + " " + cmd.getParam(1);
	msg += " :End of /NAMES list.\n";
	return (msg);
}
