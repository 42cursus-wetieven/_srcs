/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   482_ChanPrivsNeeded.cpp                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/23 16:12:25 by chervy            #+#    #+#             */
/*   Updated: 2021/10/25 19:16:46 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <Message.hpp>

std::string	Message::msgChanPrivsNeeded(Command const & cmd, Server const &server)
{
	std::string	msg;
	std::list<std::string>::const_iterator	itCmd = cmd.getCommand().begin();

	itCmd++;
	msg += ":" + server.getHost() + " " + ERR_CHANOPRIVSNEEDED_STR + " ";
	msg += cmd.getUser().getNickname() + " ";
	msg += *itCmd + " :You're not channel operator\n";

	return (msg);
}
