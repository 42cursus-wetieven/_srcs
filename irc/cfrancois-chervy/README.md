# FT_IRC

## RFC

[RFC 1459](https://datatracker.ietf.org/doc/html/rfc1459)

## Connection with `nc`

### Connect to server

```
nc 127.0.0.1 4242
```

```
PASS ******
NICK nickname
USER username * * :realname
```

## Connection with `weechat`


### Run weechat
```
weechat
```

### Add server
```
/server add ft_irc 127.0.0.1/4242 -password=******
```

### Connect to server
```
/connect ft_irc
```

## Others

### Libera
```
nc irc.libera.chat 6667
```

### Unit test
```
nc 127.0.0.1 4242 < unitTest <&1
```

### Get local ip
```
ipconfig getifaddr en0
```
