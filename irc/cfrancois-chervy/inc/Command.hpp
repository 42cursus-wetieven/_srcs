/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Command.hpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/08 17:26:08 by chervy            #+#    #+#             */
/*   Updated: 2021/10/24 18:26:44 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef COMMAND_HPP
# define COMMAND_HPP

# include <ircserv.hpp>
# include <Server.hpp>
# include <User.hpp>
# include <Channel.hpp>
# include <error.hpp>
# include <rpl.hpp>

class Command
{
	public:
		typedef void										(*cmdFct_type)(Command &,  Server &);
		typedef const std::pair<std::string, cmdFct_type>	cmdPair_type;
		typedef std::list<std::string>						command_type;
		typedef std::list< std::pair<std::string, int> >	answer_type;

		Command(Command const & command);
		Command(User & user);
		~Command();
		Command &						operator=(Command const & rhs);
		User const &					getUser() const;
		std::list<std::string> const &	getCommand() const;
		std::string						getParam(size_t n) const;
		void							setCommand(std::list<std::string> const & ref);
		void							exec(Server & server);

	private:
		static const size_t		_cmdPairSize;
		static cmdPair_type		_cmdPair[];
		User &					_user;
		command_type			_command;
		
		Command();
		void		parsing(std::string buffer);
		void		log();
		
		/*
		**	CMD
		*/
		static void	cmdPass(Command & cmd, Server & server);
		static void	cmdNick(Command & cmd, Server & server);
		static void	cmdUser(Command & cmd, Server & server);
		static void	cmdOper(Command & cmd, Server & server);
		static void	cmdQuit(Command & cmd, Server & server);
		static void	cmdJoin(Command & cmd, Server & server);
		static void	cmdPart(Command & cmd, Server & server);
		static void	cmdKick(Command & cmd, Server & server);
		static void	cmdKill(Command & cmd, Server & server);
		static void	cmdDie(Command & cmd, Server & server);
		static void	cmdNotice(Command & cmd, Server & server);
		static void	cmdPrivmsg(Command & cmd, Server & server);
		static void	cmdPing(Command & cmd, Server & server);
};

#endif