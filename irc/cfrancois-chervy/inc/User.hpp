/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   User.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/08 14:36:15 by chervy            #+#    #+#             */
/*   Updated: 2021/10/25 15:58:24 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef USER_HPP
# define USER_HPP

# include <ircserv.hpp>

class Channel;

class User
{
	public:
		User();
		User(User const & user);
		~User();

		User &						operator=(User const & rhs);
		int							operator==(User const & rhs) const;

		/*
		** Get
		*/
		int							getSockfd() const;
		std::string const &			getBuffer() const;
		std::string	const &			getHost() const;
		std::string					getNickname() const;
		std::string					getUsername() const;
		std::string					getRealname() const;
		std::list<Channel *> const	&getChans() const;

		/*
		** Is
		*/
		int							isConnected() const;
		int							isRegister();
		int							isOper() const;
		int							isAuth() const;

		/*
		** Set
		*/
		void						setSockfd(int const & sockfd);
		void						setHost(std::string const &  host);
		void						setNickname(std::string const & nickname);
		void						setUsername(std::string const & username);
		void						setRealname(std::string const & username);
		void						setOper(int is);
		void						setAuth(int is);

		/*
		** Buffer
		*/
		void						upBuffer(char const * buffer);
		int							isReadyBuffer();
		std::string					getReadyBuffer();
		void						clearBuffer();

		/*
		** Channels
		*/
		int							addChan(Channel * chan);
		void						delChan(Channel * chan);
		
		/*
		** Other
		*/
		void						deconnect();

	private:
		int						_sockfd;
		std::string				_buffer;
		int						_bufferOverflow;
		std::string 			_host;
		std::string				_nickname;
		std::string				_username;
		std::string				_realname;
		int						_connected;
		int						_register;
		int						_oper;
		int						_auth;
		std::list<Channel *>	_chans;
};

#endif