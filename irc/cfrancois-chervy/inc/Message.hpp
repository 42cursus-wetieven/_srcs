/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Message.hpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/13 18:48:24 by chervy            #+#    #+#             */
/*   Updated: 2021/10/26 12:09:46 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef MESSAGE_HPP
# define MESSAGE_HPP

# include <ircserv.hpp>
# include <Server.hpp>
# include <User.hpp>
# include <Channel.hpp>
# include <Command.hpp>

class Message
{
	public:
		typedef std::string 								(*msgFct_type)(Command const &, Server const &);
		typedef	const std::pair<int, Message::msgFct_type>	msgPair_type;
		typedef std::map<int, msgFct_type>					msgMap_type;
		Message(Message const &message);
		Message(Command const & cmd, Server const & server);
		~Message();

		void	up(std::string msg, int prefix = 0);
		void	up(int idx);
		int		send(int sockfd);
		int		send(int sockfd, int idx);
		int		send(int sockfd, std::string msg, int prefix = 0);
		void	clear();

	private:
		static const size_t	_msgMapSize;
		static msgPair_type	_msgPair[];
		static msgMap_type	_msgMap;
		Server const &		_server;
		Command const &		_cmd;
		std::string			_msg;
		
		Message();
		void	log(int const & fd, std::string const & msg) const ;

		/*
		** RPL
		*/
		static std::string	msgWelcome(Command const & cmd, Server const & server);				// 001
		static std::string	msgHost(Command const & cmd, Server const & server);				// 002
		static std::string	msgNbrUsers(Command const & cmd, Server const & server);			// 251
		static std::string	msgNameReply(Command const & cmd, Server const & server);			// 353
		static std::string	msgEndOfNames(Command const & cmd, Server const & server);			// 366

		/*
		** ERROR
		*/
		static std::string	msgNoSuchNick(Command const & cmd, Server const &);					// 401
		static std::string	msgNoSuchChannel(Command const & cmd, Server const &);				// 403
		static std::string	msgCannotSendToChan(Command const & cmd, Server const &);			// 404
		static std::string	msgTooManyChannels(Command const & cmd, Server const &);			// 405
		static std::string	msgNoRecipient(Command const & cmd, Server const &);				// 411
		static std::string	msgNoTextToSend(Command const & cmd, Server const &);				// 412
		static std::string	msgNoNicknameGiven(Command const & cmd, Server const & server);		// 431
		static std::string	msgErroneusNickname(Command const & cmd, Server const & server);	// 432
		static std::string	msgNicknameInUse(Command const & cmd, Server const & server);		// 433
		static std::string	msgNickCollision(Command const & cmd, Server const & server);		// 436
		static std::string	msgNotOnChannel(Command const & cmd, Server const &);				// 442
		static std::string	msgNotRegistered(Command const & cmd, Server const & server);		// 451
		static std::string	msgNeedMoreParams(Command const & cmd, Server const & server);		// 461
		static std::string	msgPasswdMismatch(Command const & cmd, Server const &server);		// 464
		static std::string	msgAlreadyRegistred(Command const & cmd, Server const & server);	// 462
		static std::string	msgBadChannelKey(Command const & cmd, Server const &server);		// 475
		static std::string	msgBadChanMask(Command const & cmd, Server const &);				// 476
		static std::string	msgNoPrivileges(Command const & cmd, Server const &);				// 481
		static std::string	msgChanPrivsNeeded(Command const & cmd, Server const &);			// 482
};

#endif