/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rpl.hpp                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/24 18:24:21 by chervy            #+#    #+#             */
/*   Updated: 2021/10/25 19:08:32 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef RPL_HPP
# define RPL_HPP

# define RPL_WELCOME 1
# define RPL_WELCOME_STR "001"

# define RPL_HOST 2
# define RPL_HOST_STR "002"

# define RPL_NBRUSER 251
# define RPL_NBRUSER_STR "251"

# define RPL_NAMEREPLY 353
# define RPL_NAMEREPLY_STR "353"

# define RPL_ENDOFNAMES 366
# define RPL_ENDOFNAMES_STR "366"

#endif