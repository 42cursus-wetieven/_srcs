/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ircserv.hpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/08 17:22:01 by chervy            #+#    #+#             */
/*   Updated: 2021/10/26 11:38:10 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef IRCSERV_HPP
# define IRCSERV_HPP

/*
** getaddrinfo, freeaddrinfo, connect
*/
# include <sys/types.h>

/*
** socket, setsockopt, getsockname, getaddrinfo, freeaddrinfo,
** bind, connect, listen, accept, send, recv
*/
# include <sys/socket.h>

/*
** getprotobyname, gethostbyname, getaddrinfo, freeaddrinfo
*/
# include <netdb.h>

/*
** htons, htonl, ntohs, ntohl, inet_addr, inet_ntoa
*/
# include <arpa/inet.h>

/*
** lseek
*/
# include <unistd.h>

/*
** fstat
*/
# include <sys/stat.h>

/*
** fcntl
*/
# include <fcntl.h>

/*
** poll
*/
# include <poll.h>

/*
** sockaddr_in
*/
#include <netinet/in.h>

/*
** other
*/
# include <iostream>
# include <sstream>
# include <string>
# include <list>
# include <map>
# include <vector>
# include <utility>
# include <cctype>
# include <cstdio>

class Channel;

/*
** Utils
*/
int									ft_isAlpha(char c);
int									ft_isDigit(char c);
int									ft_isWhiteSpace(char c);
int									ft_atoi(std::string str);
std::string							ft_stoa(size_t i);
std::string							ft_toUpper(std::string str);
std::list<std::string>				ft_splitInList(std::string & to_split);

#endif
