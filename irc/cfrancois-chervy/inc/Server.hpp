/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Server.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/25 19:08:35 by chervy            #+#    #+#             */
/*   Updated: 2021/10/25 19:32:39 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef SERVER_H
# define SERVER_H

typedef int SOCKET;

# include <ircserv.hpp>
# include <User.hpp>
# include <Channel.hpp>

class Server
{
	public:
		static const int	_timeout = -1;
		static const size_t	_maxConnection = 42;
		static const size_t	_maxChan = 1024;
		static const size_t	_maxChanByUser = 42;
		static const size_t	_maxLenChanName = 200;

		Server();
		Server(const int port, const std::string & psw);
		~Server();

		std::list<User> &					getUsers(void);
		const std::list<User> &				getUsers(void) const;
		std::list<Channel> &				getChannels(void);
		const std::list<Channel> &			getChannels(void) const;
		const std::string &					getHost() const;
		std::list<Channel>::iterator		getChanByName(const std::string &name);
		std::list<Channel>::const_iterator	getChanByName(const std::string &name) const;

		int									isChan(std::string chan) const;		
		int									isPassword(std::string const & psw) const;
		int									isOperPassword(std::string const & user, std::string const & psw) const;

		int									run();
		void								shutdown();

	private:

		std::list<User>		_users;
		std::list<Channel>	_chans;

		std::string			_host;
		const int 			_port;
		SOCKET 				_socket;
		sockaddr_in 		_sin;
		int					_server_status;
		std::vector<pollfd>	_fds;
		std::string			_password;
		const std::string	_oper_password;
		const std::string	_oper_user;

		int		init();
		int		new_connection();
		int		disconnect(std::vector<pollfd>::iterator const &it, User & usr);
		int		receive(int fd, User & usr);
		int		launchCommand();
		int		server_close();
		void	logConnect(int const & fd, std::string const & host);
		void	logDisconnect(int const & fd);
};

#endif
