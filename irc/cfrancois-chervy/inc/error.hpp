/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/12 15:56:42 by chervy            #+#    #+#             */
/*   Updated: 2021/10/26 12:13:44 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef ERROR_HPP
# define ERROR_HPP

# define ERR_NOSUCHNICK 401
# define ERR_NOSUCHNICK_STR "401"

# define ERR_NOSUCHCHANNEL 403
# define ERR_NOSUCHCHANNEL_STR "403"

# define ERR_CANNOTSENDTOCHAN 404
# define ERR_CANNOTSENDTOCHAN_STR "404"

# define ERR_TOOMANYCHANNELS 405
# define ERR_TOOMANYCHANNELS_STR "405"

# define ERR_NORECIPIENT 411
# define ERR_NORECIPIENT_STR "411"

# define ERR_NOTEXTTOSEND 412
# define ERR_NOTEXTTOSEND_STR "412"

# define ERR_NONICKNAMEGIVEN 431
# define ERR_NONICKNAMEGIVEN_STR "431"

# define ERR_ERRONEUSNICKNAME 432
# define ERR_ERRONEUSNICKNAME_STR "432"

# define ERR_NICKNAMEINUSE 433
# define ERR_NICKNAMEINUSE_STR "433"

# define ERR_NICKCOLLISION 436
# define ERR_NICKCOLLISION_STR "436"

# define ERR_NOTONCHANNEL 442
# define ERR_NOTONCHANNEL_STR "442"

# define ERR_NOTREGISTERED 451
# define ERR_NOTREGISTERED_STR "451"

# define ERR_NEEDMOREPARAMS 461
# define ERR_NEEDMOREPARAMS_STR "461"

# define ERR_ALREADYREGISTRED 462
# define ERR_ALREADYREGISTRED_STR "462"

# define ERR_PASSWDMISMATCH 464
# define ERR_PASSWDMISMATCH_STR "464"

# define ERR_BADCHANNELKEY 475
# define ERR_BADCHANNELKEY_STR "475"

# define ERR_BADCHANMASK 476
# define ERR_BADCHANMASK_STR "476"

# define ERR_NOPRIVILEGES 481
# define ERR_NOPRIVILEGES_STR "481"

# define ERR_CHANOPRIVSNEEDED 482
# define ERR_CHANOPRIVSNEEDED_STR "482"

# define ERR_INVALIDUSERNAME 512
# define ERR_INVALIDUSERNAME_STR "512"

#endif