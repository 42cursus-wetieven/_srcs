/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Channel.hpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chervy <chervy@student.42lyon.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/08 16:20:23 by chervy            #+#    #+#             */
/*   Updated: 2021/10/24 18:24:07 by chervy           ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef CHANNEL_HPP
# define CHANNEL_HPP

# include <ircserv.hpp>
# include <User.hpp>

class Channel
{
	public:
		typedef std::list< std::pair<User &, int> >					users_type;
		typedef std::list< std::pair<User &, int> >::iterator		users_iterator;
		typedef std::list< std::pair<User &, int> >::const_iterator	users_const_iterator;

		Channel(Channel const & channel);
		Channel(std::string const & name, std::string const & key);
		~Channel();
		Channel &				operator=(Channel const & rhs);
		std::string				getName(void) const;
		users_type const &		getUsers(void) const;
		users_const_iterator	getUserByNickname(const std::string & nick) const;
		void					setGrade(User const & user, int const & grade);
		int						isRegister(User const & user);
		int						isOper(User const & user);
		int						addUser(User & user, std::string const & key);
		void					delUser(User & user);
		
	private:
		std::string	_name;
		std::string	_key;
		users_type	_users;
		
		Channel();
};

#endif