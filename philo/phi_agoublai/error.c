/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agoublai <agoublai@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/29 19:27:26 by agoublai          #+#    #+#             */
/*   Updated: 2021/09/13 17:27:38 by agoublai         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

static void	destroy_forks(t_rules *rules)
{
	int	i;

	i = 0;
	while (i < rules->phi_n)
	{
		pthread_mutex_unlock(&rules->forks[i]);
		pthread_mutex_destroy(&rules->forks[i]);
		i++;
	}
}

void	free_all(t_rules *rules)
{
	if (rules->phi)
		free(rules->phi);
	if (rules->forks)
	{
		destroy_forks(rules);
		free(rules->forks);
	}
	pthread_mutex_unlock(&rules->state);
	pthread_mutex_destroy(&rules->state);
}
