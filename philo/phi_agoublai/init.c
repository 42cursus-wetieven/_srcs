/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agoublai <agoublai@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/29 19:24:40 by agoublai          #+#    #+#             */
/*   Updated: 2021/09/27 16:36:08 by agoublai         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

static void	init_philos(t_rules *rules)
{
	int	i;

	i = 0;
	while (i < rules->phi_n)
	{
		rules->phi[i].id = i + 1;
		rules->phi[i].rfork = i;
		if (i == 0)
			rules->phi[i].lfork = rules->phi_n - 1;
		else
			rules->phi[i].lfork = i - 1;
		rules->phi[i].rules = rules;
		rules->phi[i].meals = 0;
		rules->phi[i].last_meal = get_time();
		pthread_create(&rules->phi[i].thread, NULL, th_philo, &rules->phi[i]);
		i++;
	}
}

static void	init_forks(t_rules *rules)
{
	int	i;

	i = 0;
	while (i < rules->phi_n)
	{
		pthread_mutex_init(&rules->forks[i], NULL);
		i++;
	}
}

void	init_last_meal(t_rules *rules)
{
	int	i;

	i = 0;
	while (i < rules->phi_n)
	{
		rules->phi[i].last_meal = rules->ds_time;
		i++;
	}
}

void	init_rules(t_rules *rules, int ac, char **av)
{
	rules->rdy = 0;
	rules->phi_n = ft_atoi(av[1]);
	rules->death_t = ft_atoi(av[2]);
	rules->eat_t = ft_atoi(av[3]);
	rules->sleep_t = ft_atoi(av[4]);
	if (ac == 6)
		rules->max_meal = ft_atoi(av[5]);
	else
		rules->max_meal = -1;
	if (rules->phi_n == 0 || rules->death_t == 0
		|| rules->eat_t == 0 || rules->sleep_t == 0 || rules->max_meal == 0)
		return ;
	rules->all_ate = 0;
	rules->dead_phi = 0;
	pthread_mutex_init(&rules->state, NULL);
	pthread_mutex_init(&rules->eating, NULL);
	rules->forks = malloc(sizeof(pthread_mutex_t) * rules->phi_n);
	init_forks(rules);
	rules->phi = (t_philo *)malloc(sizeof(t_philo) * rules->phi_n);
	init_philos(rules);
	rules->ds_time = get_time();
	rules->rdy = 1;
	init_last_meal(rules);
}
