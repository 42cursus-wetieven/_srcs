/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agoublai <agoublai@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/29 16:39:41 by agoublai          #+#    #+#             */
/*   Updated: 2021/09/27 16:55:37 by agoublai         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILO_H
# define PHILO_H

# include <pthread.h>
# include <stdlib.h>
# include <unistd.h>
# include <sys/time.h>

# include <stdio.h>

typedef struct s_philo
{
	int				id;
	int				rfork;
	int				lfork;
	int				meals;
	long long		last_meal;
	pthread_t		thread;
	struct s_rules	*rules;
}				t_philo;

typedef struct s_rules
{
	int				phi_n;
	int				death_t;
	int				eat_t;
	int				sleep_t;
	int				max_meal;
	int				rdy;
	int				dead_phi;
	int				all_ate;
	long long		ds_time;
	t_philo			*phi;
	pthread_mutex_t	*forks;
	pthread_mutex_t	state;
	pthread_mutex_t	eating;
}				t_rules;

// main.c
void		*th_philo(void *arg);

// init.c
void		init_rules(t_rules *rules, int ac, char **av);

// error.c
void		free_all(t_rules *rules);

// utils.c
void		printa(char *s, int id, t_rules *rules);
long long	get_time(void);
void		acc_sleep(long long check);
int			ft_atoi(char *s);

#endif