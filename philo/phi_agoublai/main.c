/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agoublai <agoublai@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/29 16:39:25 by agoublai          #+#    #+#             */
/*   Updated: 2021/10/01 11:19:22 by agoublai         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

void	eat(t_philo	*philo)
{
	if (philo->id % 2 == 0)
	{
		pthread_mutex_lock(&philo->rules->forks[philo->rfork]);
		printa("has taken a fork", philo->id, philo->rules);
		pthread_mutex_lock(&philo->rules->forks[philo->lfork]);
		printa("has taken a fork", philo->id, philo->rules);
	}
	else
	{
		pthread_mutex_lock(&philo->rules->forks[philo->lfork]);
		printa("has taken a fork", philo->id, philo->rules);
		if (philo->rules->phi_n == 1)
			return ;
		pthread_mutex_lock(&philo->rules->forks[philo->rfork]);
		printa("has taken a fork", philo->id, philo->rules);
	}
	pthread_mutex_lock(&philo->rules->eating);
	philo->last_meal = get_time();
	pthread_mutex_unlock(&philo->rules->eating);
	printa("is eating", philo->id, philo->rules);
	acc_sleep(philo->rules->eat_t);
	pthread_mutex_unlock(&philo->rules->forks[philo->rfork]);
	pthread_mutex_unlock(&philo->rules->forks[philo->lfork]);
}

void	*th_philo(void *arg)
{
	t_philo	*phi;

	phi = (t_philo *)arg;
	while (!phi->rules->rdy)
		;
	phi->last_meal = phi->rules->ds_time;
	while (!phi->rules->dead_phi)
	{
		eat(phi);
		phi->meals++;
		if (phi->meals == phi->rules->max_meal && phi->rules->phi_n > 1)
			phi->rules->all_ate++;
		if (phi->rules->all_ate == phi->rules->phi_n
			|| phi->rules->phi_n == 1 || phi->rules->dead_phi)
			return (NULL);
		printa("is sleeping", phi->id, phi->rules);
		acc_sleep(phi->rules->sleep_t);
		if (phi->rules->all_ate == phi->rules->phi_n || phi->rules->dead_phi)
			return (NULL);
		printa("is thinking", phi->id, phi->rules);
	}
	return (NULL);
}

void	check_death(t_philo *philo)
{
	int	i;

	while (!philo->rules->dead_phi
		&& philo->rules->all_ate != philo->rules->phi_n)
	{
		i = 0;
		while (i < philo->rules->phi_n)
		{
			pthread_mutex_lock(&philo->rules->eating);
			if (get_time() - philo[i].last_meal > philo->rules->death_t
				&& philo->rules->all_ate != philo->rules->phi_n)
			{
				philo->rules->dead_phi = 1;
				printa("is dead", i + 1, philo->rules);
				pthread_mutex_unlock(&philo->rules->eating);
				return ;
			}
			pthread_mutex_unlock(&philo->rules->eating);
			i++;
		}
	}
	usleep(50);
}

int	main(int ac, char **av)
{
	t_rules	rules;
	int		i;

	i = 0;
	if (ac == 5 || ac == 6)
	{
		if (ac == 6 && av[5][0] == '0' && !av[5][1])
			return (0);
		init_rules(&rules, ac, av);
		if (rules.phi_n == 0 || rules.death_t == 0
			|| rules.eat_t == 0 || rules.sleep_t == 0 || rules.max_meal == 0)
			return (0);
		check_death(rules.phi);
		while (i < rules.phi_n)
		{
			if (rules.phi->thread)
				pthread_join(rules.phi[i].thread, NULL);
			i++;
		}
		free_all(&rules);
	}
	return (0);
}
