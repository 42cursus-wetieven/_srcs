/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agoublai <agoublai@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/29 20:11:57 by agoublai          #+#    #+#             */
/*   Updated: 2021/10/01 13:42:23 by agoublai         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

void	printa(char *s, int id, t_rules *rules)
{
	pthread_mutex_lock(&rules->state);
	if (rules->all_ate != rules->phi_n && !rules->dead_phi)
	{
		printf("%lli ", get_time() - rules->ds_time);
		printf("%d %s\n", id, s);
	}
	else if (s[3] == 'd')
	{
		printf("%lli ", get_time() - rules->ds_time);
		printf("%d %s\n", id, s);
	}
	pthread_mutex_unlock(&rules->state);
}

long long	get_time(void)
{
	struct timeval	time;

	gettimeofday(&time, NULL);
	return (time.tv_sec * 1000 + time.tv_usec / 1000);
}

void	acc_sleep(long long check)
{
	long long	time;

	time = get_time();
	while (get_time() - time < check)
		usleep(50);
}

int	ft_atoi(char *s)
{
	int		i;
	int		res;

	res = 0;
	i = 0;
	while (s[i] >= '0' && s[i] <= '9')
	{
		res = res * 10 + (s[i] - '0');
		i++;
	}
	if (s[i])
		return (0);
	return (res);
}
